#   ____          _   _      
#  / ___|__ _ ___| |_| | ___ 
# | |   / _` / __| __| |/ _ \
# | |__| (_| \__ \ |_| |  __/
#  \____\__,_|___/\__|_|\___|
# 
# Extensible Automation Orchestration.
# Copyright (C) 2019 Zach Podbielniak
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=gnu89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
WARNINGS += -Wno-unused-label
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE
ifeq ($(BUILD_TYPE),onesdk)
DEFINES += -D BUILD_WITH_ONEAGENT_SDK
endif

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__
ifeq ($(BUILD_TYPE),onesdk)
DEFINES_D += -D BUILD_WITH_ONEAGENT_SDK
endif

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)
#CC_FLAGS_D += -fsanitize=address

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall


FILES = Automaton.o AutomatonRegistry.o AutomatonRest.o
FILES += CastleServer.o 
FILES += TestAutomaton.o TextAutomaton.o PublicIPClientAutomaton.o
FILES += SwitchableDeviceAutomaton.o


FILES_D = Automaton_d.o AutomatonRegistry_d.o AutomatonRest_d.o 
FILES_D += CastleServer_d.o
FILES_D += TestAutomaton_d.o TextAutomaton_d.o PublicIPClientAutomaton_d.o
FILES_D += SwitchableDeviceAutomaton_d.o


all: 	bin libcastle.so 
debug:	bin libcastle_d.so
test:	bin $(TEST)

bin:
	mkdir -p bin/

clean:	bin 
	rm -rf bin/

install:
	cp bin/libcastle.so /usr/lib/

install_debug:
	cp bin/libcastle_d.so /usr/lib/

install_headers:
	rm -rf /usr/include/Castle/
	mkdir -p /usr/include/Castle
	find . -type f -name "*.h" -exec install -D {} /usr/include/Castle/{} \;
	mv /usr/include/Castle/Src/* /usr/include/Castle
	rm -rf /usr/include/Castle/Src





docs:
	robodoc --src Src/ --doc Docs/ --multidoc --html --syntaxcolors --nosort

libcastle.so:	$(FILES)
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -shared -fPIC -s -o bin/libcastle.so bin/*.o -pthread -lpodnet -lcrock -lpodmq -lpodmqhttp -lonesdk_shared $(STD) $(OPTIMIZE) $(MARCH) $(CC_FLAGS)
else 
	$(CC) -shared -fPIC -s -o bin/libcastle.so bin/*.o -pthread -lpodnet -lcrock -lpodmq -lpodmqhttp $(STD) $(OPTIMIZE) $(MARCH) $(CC_FLAGS)
endif
	rm bin/*.o

libcastle_d.so:	$(FILES_D)
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -g -shared -fPIC -o bin/libcastle_d.so bin/*_d.o -pthread -lpodnet_d -lcrock_d -lpodmq_d -lpodmqhttp_d -lonesdk_shared $(STD) $(CC_FLAGS_D)
else
	$(CC) -g -shared -fPIC -o bin/libcastle_d.so bin/*_d.o -pthread -lpodnet_d -lcrock_d -lpodmq_d -lpodmqhttp_d $(STD) $(CC_FLAGS_D)
endif
	rm bin/*.o

host: bin 
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -fPIC -o bin/Castle Src/ServerHost.c -lpodnet -lcrock -lcastle -lpodmq -lpodmqhttp -lonesdk_shared $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH) $(CC_FLAGS)
else
	$(CC) -fPIC -o bin/Castle Src/ServerHost.c -lpodnet -lcrock -lcastle -lpodmq -lpodmqhttp $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH) $(CC_FLAGS)
endif

host_d: bin 
ifeq ($(BUILD_TYPE),onesdk)
	$(CC) -g -fPIC -o bin/Castle Src/ServerHost.c -lpodnet_d -lcrock_d -lcastle_d -lpodmq_d -lpodmqhttp_d -lonesdk_shared $(CC_FLAGS_D) 
else
	$(CC) -g -fPIC -o bin/Castle Src/ServerHost.c -lpodnet_d -lcrock_d -lcastle_d -lpodmq_d -lpodmqhttp_d $(CC_FLAGS_D) 
endif





Automaton.o:
	$(CC) -fPIC -c -o bin/Automaton.o Src/Automaton/Automaton.c $(CC_FLAGS)

AutomatonRegistry.o:
	$(CC) -fPIC -c -o bin/AutomatonRegistry.o Src/Automaton/AutomatonRegistry.c $(CC_FLAGS)

AutomatonRest.o:
	$(CC) -fPIC -c -o bin/AutomatonRest.o Src/Automaton/AutomatonRest.c $(CC_FLAGS)

CastleServer.o:
	$(CC) -fPIC -c -o bin/CastleServer.o Src/Server/CastleServer.c $(CC_FLAGS)

TestAutomaton.o:
	$(CC) -fPIC -c -o bin/TestAutomaton.o Src/Automaton/TestAutomaton.c $(CC_FLAGS)

TextAutomaton.o:
	$(CC) -fPIC -c -o bin/TextAutomaton.o Src/Automaton/TextAutomaton.c $(CC_FLAGS)

PublicIPClientAutomaton.o:
	$(CC) -fPIC -c -o bin/PublicIPAutomaton.o Src/Automaton/PublicIPClientAutomaton.c $(CC_FLAGS);

SwitchableDeviceAutomaton.o:
	$(CC) -fPIC -c -o bin/SwitchableDeviceAutomaton.o Src/Automaton/SwitchableDeviceAutomaton.c $(CC_FLAGS);





Automaton_d.o:
	$(CC) -g -fPIC -c -o bin/Automaton_d.o Src/Automaton/Automaton.c $(CC_FLAGS_D)

AutomatonRegistry_d.o:
	$(CC) -g -fPIC -c -o bin/AutomatonRegistry_d.o Src/Automaton/AutomatonRegistry.c $(CC_FLAGS_D)

AutomatonRest_d.o:
	$(CC) -g -fPIC -c -o bin/AutomatonRest_d.o Src/Automaton/AutomatonRest.c $(CC_FLAGS_D)

CastleServer_d.o:
	$(CC) -g -fPIC -c -o bin/CastleServer_d.o Src/Server/CastleServer.c $(CC_FLAGS_D)

TestAutomaton_d.o:
	$(CC) -g -fPIC -c -o bin/TestAutomaton_d.o Src/Automaton/TestAutomaton.c $(CC_FLAGS_D)

TextAutomaton_d.o:
	$(CC) -g -fPIC -c -o bin/TextAutomaton_d.o Src/Automaton/TextAutomaton.c $(CC_FLAGS_D)

PublicIPClientAutomaton_d.o:
	$(CC) -g -fPIC -c -o bin/PublicIPAutomaton_d.o Src/Automaton/PublicIPClientAutomaton.c $(CC_FLAGS_D);

SwitchableDeviceAutomaton_d.o:
	$(CC) -g -fPIC -c -o bin/SwitchableDeviceAutomaton_d.o Src/Automaton/SwitchableDeviceAutomaton.c $(CC_FLAGS_D);
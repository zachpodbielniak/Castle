/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "AutomatonRegistry.h"




typedef struct __AUTOMATON_REGISTRY
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	HBINARYSEARCHTREE	hbstData;
	LPSTR 			lpszName;
	UARCHLONG 		ualDeviceCount;
} AUTOMATON_REGISTRY, *LPAUTOMATON_REGISTRY, **DLPAUTOMATON_REGISTRY;




typedef struct __REGISTRY_SEARCH_PARAMS
{
	LPSTR 		lpszName;
	LPSTR 		lpszClass;
	HANDLE 		hAutomatonOut;
	HANDLE 		hRegistryOut;
	HVECTOR 	hvOut;
} REGISTRY_SEARCH_PARAMS, *LPREGISTRY_SEARCH_PARAMS;




GLOBAL_VARIABLE HANDLE hDefault = NULL_OBJECT;
GLOBAL_VARIABLE HBINARYSEARCHTREE hbstOther = NULLPTR;




_Call_Back_
INTERNAL_OPERATION
BOOL 
__DestroyAutomatonRegistry(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPAUTOMATON_REGISTRY lpatData;
	lpatData = hDerivative;

	WaitForSingleObject(lpatData->hLock, INFINITE_WAIT_TIME);
	DestroyBinarySearchTree(lpatData->hbstData);
	DestroyObject(lpatData->hLock);
	FreeMemory(lpatData);
	
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE
CreateAutomatonRegistry(
	_In_Z_ 		LPCSTR 				lpcszName
){
	HANDLE hRegistry;
	LPAUTOMATON_REGISTRY lpatData;

	lpatData = GlobalAllocAndZero(sizeof(AUTOMATON_REGISTRY));
	if (NULLPTR == lpatData)
	{ JUMP(__CREATE_ALLOC_FAILED); }

	lpatData->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lpatData->hLock)
	{ JUMP(__CREATE_CRITSEC_FAILED); }

	lpatData->hbstData = CreateBinarySearchTree(
		TYPE_UARCHLONG,
		sizeof(HANDLE),
		NULLPTR,
		(LPFN_BS_TREE_DATA_FREE_PROC)DestroyObject	/* Magic */
	);

	if (NULLPTR == lpatData->hbstData)
	{ JUMP(__CREATE_BS_TREE_FAILED); }
	
	hRegistry = CreateHandleWithSingleInheritor(
		lpatData,
		&(lpatData->hThis),
		HANDLE_TYPE_AUTOMATON_REGISTRY,
		__DestroyAutomatonRegistry,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpatData->ullId),
		0
	);

	if (NULL_OBJECT == hRegistry)
	{ JUMP(__CREATE_OBJECT_FAILED); }

	/* Internal Reference Keeping Logic */
	if (NULLPTR == lpcszName)
	{ 
		hDefault = hRegistry; 
		lpatData->lpszName = StringDuplicate("Default");
	}
	else if (0 == StringCompare("Default", lpcszName))
	{
		hDefault = hRegistry;
		lpatData->lpszName = StringDuplicate(lpcszName);
	}
	else
	{
		if (NULLPTR == hbstOther)
		{ 
			hbstOther = CreateBinarySearchTree(
				TYPE_UARCHLONG,
				sizeof(HANDLE),
				NULLPTR,
				(LPFN_BS_TREE_DATA_FREE_PROC)DestroyObject
			);
		}

		BinarySearchTreeInsert(
			hbstOther,
			(LPVOID)&hRegistry,
			sizeof(HANDLE)
		);

		lpatData->lpszName = StringDuplicate(lpcszName);
	}

	return hRegistry;
	
	__CREATE_OBJECT_FAILED:
	DestroyBinarySearchTree(lpatData->hbstData);
	__CREATE_BS_TREE_FAILED:
	DestroyObject(lpatData->hLock);
	__CREATE_CRITSEC_FAILED:
	FreeMemory(lpatData);
	__CREATE_ALLOC_FAILED:
	return NULL_OBJECT;
}




_Success_ (return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__SearchForRegistryName(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);

	HANDLE hAutomaton;
	LPAUTOMATON_REGISTRY lpatData;
	LPREGISTRY_SEARCH_PARAMS lprspData;

	lprspData = lpUserData;
	hAutomaton = *(LPHANDLE)lpData;
	lpatData = OBJECT_CAST(hAutomaton, LPAUTOMATON_REGISTRY);

	if (0 == StringCompare(lprspData->lpszName, lpatData->lpszName))
	{ lprspData->hRegistryOut = *(LPHANDLE)lpData; }

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE 
AutomatonGetRegistryByName(
	_In_Z_ 		LPCSTR 				lpcszName
){
	if (NULLPTR == lpcszName)
	{ return hDefault; }
	else if (0 == StringCompare("Default", lpcszName))
	{ return hDefault; }
	else
	{
		REGISTRY_SEARCH_PARAMS rspData;
		ZeroMemory(&rspData, sizeof(REGISTRY_SEARCH_PARAMS));

		rspData.lpszName = (LPSTR)lpcszName;

		if (NULLPTR != hbstOther)
		{
			BinarySearchTreeTraverse(
				hbstOther,
				__SearchForRegistryName,
				&rspData
			);
		}

		return rspData.hRegistryOut;
	}

	return NULL_OBJECT; 
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
LPCSTR
AutomatonGetRegistryName(
	_In_ 		HANDLE 				hRegistry
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, NULL_OBJECT);

	LPAUTOMATON_REGISTRY lpatRegistry;
	lpatRegistry = OBJECT_CAST(hRegistry, LPAUTOMATON_REGISTRY);
	EXIT_IF_UNLIKELY_NULL(lpatRegistry, NULL_OBJECT);

	return (LPCSTR)StringDuplicate(lpatRegistry->lpszName);
}




_Success_ (return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__GetAllRegistries(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);

	HANDLE hRegistry;
	LPREGISTRY_SEARCH_PARAMS lprspData;

	hRegistry = *(LPHANDLE)lpData;
	lprspData = lpUserData;

	VectorPushBack(lprspData->hvOut, &hRegistry, 0); 

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API 
HVECTOR
AutomatonGetAllRegistries(
	VOID
){
	REGISTRY_SEARCH_PARAMS rspData;
	ZeroMemory(&rspData, sizeof(REGISTRY_SEARCH_PARAMS));

	rspData.hvOut = CreateVector(0x08, sizeof(HANDLE), 0);

	if (NULL_OBJECT != hDefault)
	{ VectorPushBack(rspData.hvOut, &hDefault, 0); }

	if (NULL != hbstOther)
	{
		BinarySearchTreeTraverse(
			hbstOther,
			__GetAllRegistries,
			&rspData
		);
	}

	return rspData.hvOut;
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
UARCHLONG
AutomatonRegistryGetDeviceCount(
	_In_ 		HANDLE 				hRegistry
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, (UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, (UARCHLONG)-1);

	LPAUTOMATON_REGISTRY lpatData;
	UARCHLONG ualRet;
	
	lpatData = OBJECT_CAST(hRegistry, LPAUTOMATON_REGISTRY);
	EXIT_IF_UNLIKELY_NULL(lpatData, FALSE);

	WaitForSingleObject(lpatData->hLock, INFINITE_WAIT_TIME);
	ualRet = lpatData->ualDeviceCount;
	ReleaseSingleObject(lpatData->hLock);

	return ualRet;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL
AutomatonRegistryTraverse(
	_In_ 		HANDLE 				hRegistry,
	_In_ 		LPFN_BS_TREE_TRAVERSE_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 				lpData
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnCallBack, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, FALSE);

	LPAUTOMATON_REGISTRY lpatData;
	BOOL bRet;

	lpatData = OBJECT_CAST(hRegistry, LPAUTOMATON_REGISTRY);
	EXIT_IF_UNLIKELY_NULL(lpatData, FALSE);

	WaitForSingleObject(lpatData->hLock, INFINITE_WAIT_TIME);
	bRet = BinarySearchTreeTraverse(lpatData->hbstData, lpfnCallBack, lpData);
	ReleaseSingleObject(lpatData->hLock);

	return bRet;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegistryAdd(
	_In_ 		HANDLE RESTRICT			hRegistry,
	_In_ 		HANDLE RESTRICT			hAutomaton
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, FALSE);
	EXIT_IF_UNLIKELY_NULL(hRegistry, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hAutomaton), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON_REGISTRY lpatData;
	BOOL bRet;

	lpatData = OBJECT_CAST(hRegistry, LPAUTOMATON_REGISTRY);
	EXIT_IF_UNLIKELY_NULL(lpatData, FALSE);

	WaitForSingleObject(lpatData->hLock, INFINITE_WAIT_TIME);
	bRet = BinarySearchTreeInsert(lpatData->hbstData, (LPVOID)&hAutomaton, sizeof(HANDLE));
	lpatData->ualDeviceCount++;
	ReleaseSingleObject(lpatData->hLock);

	return bRet;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegistryRemove(
	_In_ 		HANDLE RESTRICT			hRegistry,
	_In_ 		HANDLE RESTRICT			hAutomaton
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, FALSE);
	EXIT_IF_UNLIKELY_NULL(hRegistry, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hAutomaton), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON_REGISTRY lpatData;
	LPVOID lpNode;
	BOOL bRet;

	lpatData = OBJECT_CAST(hRegistry, LPAUTOMATON_REGISTRY);
	EXIT_IF_UNLIKELY_NULL(lpatData, FALSE);

	WaitForSingleObject(lpatData->hLock, INFINITE_WAIT_TIME);
	lpNode = BinarySearchTreeSearch(lpatData->hbstData, (LPVOID)&hAutomaton, sizeof(HANDLE));
	if (NULLPTR == lpNode)
	{ bRet = FALSE; }
	else 
	{ 
		bRet = BinarySearchTreeDestroyAt(lpatData->hbstData, lpNode); 
		lpatData->ualDeviceCount--;
	}
	ReleaseSingleObject(lpatData->hLock);

	return bRet;
}




/* TODO: Implement */
_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegistryRemoveByClassAndName(
	_In_ 		HANDLE RESTRICT			hRegistry,
	_In_Z_ 		LPCSTR RESTRICT			lpcszDeviceClass,
	_In_Z_ 		LPCSTR RESTRICT			lpcszDeviceName
);




_Success_ (return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__AutomatonIterateDoTickOnAll(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hAutomaton;
	#ifdef BUILD_WITH_ONEAGENT_SDK
	CSTRING csFullDeviceName[0x100];
	onesdk_tracer_handle_t osthTick;
	onesdk_tracer_handle_t osthRead;
	onesdk_tracer_handle_t osthWrite;
	#endif


	hAutomaton = *(LPHANDLE)lpData;


	if (NULL_OBJECT == hAutomaton)
	{ return NULLPTR; }

	#ifdef BUILD_WITH_ONEAGENT_SDK
	ZeroMemory(csFullDeviceName, sizeof(csFullDeviceName));

	StringPrintFormatSafe(
		csFullDeviceName,
		sizeof(csFullDeviceName) - 0x01U,
		"%s:%s",
		AutomatonGetClass(hAutomaton),
		AutomatonGetName(hAutomaton)
	);

	osthTick = onesdk_customservicetracer_create(
		onesdk_asciistr("AutomatonTick"),
		onesdk_asciistr(csFullDeviceName)
	);
	#endif

	AutomatonTick(hAutomaton, NULLPTR, 0);

	if (FALSE != AutomatonTriggeredRead(hAutomaton))
	{
		#ifdef BUILD_WITH_ONEAGENT_SDK
		osthRead = onesdk_customservicetracer_create(
			onesdk_asciistr("AutomatonTickRead"),
			onesdk_asciistr(csFullDeviceName)
		);

		onesdk_tracer_start(osthRead);
		#endif

		AutomatonReadEx(
			hAutomaton,
			NULLPTR,
			0,
			NULLPTR,
			NULLPTR,
			NULLPTR,
			AUTOMATON_READ_TRIGGERED_BY_TICK
		);

		#ifdef BUILD_WITH_ONEAGENT_SDK
		onesdk_tracer_end(osthRead);
		#endif
	}
	
	if (FALSE != AutomatonTriggeredWrite(hAutomaton))
	{
		#ifdef BUILD_WITH_ONEAGENT_SDK
		osthWrite = onesdk_customservicetracer_create(
			onesdk_asciistr("AutomatonTickWrite"),
			onesdk_asciistr(csFullDeviceName)
		);

		onesdk_tracer_start(osthRead);
		#endif
		
		AutomatonWriteEx(
			hAutomaton,
			NULLPTR,
			0,
			TYPE_LPVOID,
			NULLPTR,
			AUTOMATON_WRITE_TRIGGERED_BY_TICK
		);
		
		#ifdef BUILD_WITH_ONEAGENT_SDK
		onesdk_tracer_end(osthWrite);
		#endif
	}

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_end(osthTick);
	#endif

	return NULLPTR;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API 
BOOL 
AutomatonRegistryPerformTickOnAll(
	_In_ 		HANDLE RESTRICT 		hRegistry
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, FALSE);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_handle_t osthTick;

	osthTick = onesdk_customservicetracer_create(
		onesdk_asciistr("AutomatonRegistryTick"),
		onesdk_asciistr("Automaton Registry Tick Master")
	);

	onesdk_tracer_start(osthTick);
	#endif

	AutomatonRegistryTraverse(hRegistry, __AutomatonIterateDoTickOnAll, NULLPTR);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	onesdk_tracer_end(osthTick);
	#endif

	return TRUE;
}




_Success_ (return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__AutomatonRegistrySearchTreeCallback(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hAutomaton;
	LPREGISTRY_SEARCH_PARAMS lprspData;

	hAutomaton = *(LPHANDLE)lpData;
	lprspData = lpUserData;

	if (NULL_OBJECT == hAutomaton)
	{ return NULLPTR; }

	if (0 == StringCompare(lprspData->lpszClass, AutomatonGetClass(hAutomaton)))
	{
		if (0 == StringCompare(lprspData->lpszName, AutomatonGetName(hAutomaton)))
		{ lprspData->hAutomatonOut = hAutomaton; }
	}

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
INTERNAL_OPERATION
HANDLE 
__AutomatonRegistrySearchNoLock(
	_In_ 		LPAUTOMATON_REGISTRY		lpatData,
	_In_Z_ 		LPCSTR RESTRICT 		lpcszClass,
	_In_Z_		LPCSTR RESTRICT 		lpcszName
){
	EXIT_IF_UNLIKELY_NULL(lpatData, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszClass, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszName, NULL_OBJECT);

	REGISTRY_SEARCH_PARAMS rspData;
	ZeroMemory(&rspData, sizeof(REGISTRY_SEARCH_PARAMS));

	rspData.lpszClass = (LPSTR)lpcszClass;
	rspData.lpszName = (LPSTR)lpcszName;

	BinarySearchTreeTraverse(
		lpatData->hbstData,
		__AutomatonRegistrySearchTreeCallback,
		&rspData
	);

	return rspData.hAutomatonOut;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE 
AutomatonRegistrySearch(
	_In_ 		HANDLE RESTRICT 		hRegistry,
	_In_Z_ 		LPCSTR RESTRICT 		lpcszClass,
	_In_Z_		LPCSTR RESTRICT 		lpcszName
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, NULL_OBJECT);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, NULL_OBJECT);

	HANDLE hAutomaton;
	LPAUTOMATON_REGISTRY lpatData;

	lpatData = OBJECT_CAST(hRegistry, LPAUTOMATON_REGISTRY);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULL_OBJECT);

	WaitForSingleObject(lpatData->hLock, INFINITE_WAIT_TIME);
	hAutomaton = __AutomatonRegistrySearchNoLock(lpatData, lpcszClass, lpcszName);
	ReleaseSingleObject(lpatData->hLock);

	return hAutomaton;
}
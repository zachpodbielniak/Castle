/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CASTLE_GLOBALAUTOMATONREGISTRY_H
#define CASTLE_GLOBALAUTOMATONREGISTRY_H


#include "Automaton.h"


_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE
CreateAutomatonRegistry(
	_In_Z_ 		LPCSTR 				lpcszName
);




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE 
AutomatonGetRegistryByName(
	_In_Z_ 		LPCSTR 				lpcszName
);




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
LPCSTR
AutomatonGetRegistryName(
	_In_ 		HANDLE 				hRegistry
);




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API 
HVECTOR
AutomatonGetAllRegistries(
	VOID
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
UARCHLONG
AutomatonRegistryGetDeviceCount(
	_In_ 		HANDLE 				hRegistry
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL
AutomatonRegistryTraverse(
	_In_ 		HANDLE 				hRegistry,
	_In_ 		LPFN_BS_TREE_TRAVERSE_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 				lpData
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegistryAdd(
	_In_ 		HANDLE RESTRICT			hRegistry,
	_In_ 		HANDLE RESTRICT			hAutomaton
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegistryRemove(
	_In_ 		HANDLE RESTRICT			hRegistry,
	_In_ 		HANDLE RESTRICT			hAutomaton
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegistryRemoveByClassAndName(
	_In_ 		HANDLE RESTRICT			hRegistry,
	_In_Z_ 		LPCSTR RESTRICT			lpcszDeviceClass,
	_In_Z_ 		LPCSTR RESTRICT			lpcszDeviceName
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API 
BOOL 
AutomatonRegistryPerformTickOnAll(
	_In_ 		HANDLE RESTRICT 		hRegistry
);



_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE 
AutomatonRegistrySearch(
	_In_ 		HANDLE RESTRICT 		hRegistry,
	_In_Z_ 		LPCSTR RESTRICT 		lpcszClass,
	_In_Z_		LPCSTR RESTRICT 		lpcszName
);


#endif

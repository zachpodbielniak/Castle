/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "AutomatonRest.h"


LPSTR lpszHtmlHeadStart = 	"<!DOCTYPE html>\r\n"
				"<html>\r\n"
				"<head>\r\n"
				"<title>Castle -- Automation</title>\r\n"
				"<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'></link>\r\n"
				"<style>\r\n"
				"table, th, td {\r\n"
				"border: 1px solid black;\r\n"
				"border-collapse: collapse;\r\n"
				"padding: 15px;\r\n"
				"}\r\n"
				"a {\r\n"
				"color: blue;\r\n"
				"}\r\n"
				"a:link, a:visited {\r\n"
				"color: white;\r\n"
				"}\r\n"
				"a:hover {\r\n"
				"color: red;\r\n"
				"}\r\n"
				"</style>\r\n"
				"<script type='text/javascript'>\r\n"
				"function DoReloadHandler(e) {\r\n"
				"location.reload();\r\n"
				"}\r\n"
				"function DoXhrGet(sUrl) {\r\n"
				"xReq = new XMLHttpRequest();\r\n"
				"xReq.addEventListener('loadend', DoReloadHandler);\r\n"
				"xReq.open(\"GET\", sUrl, true);\r\n"
				"xReq.send();\r\n"
				"}\r\n"
				"</script>\r\n";
LPSTR lpszHtmlHeadEnd = 	"</head>\r\n";

LPSTR lpszHtmlBodyStart = 	"<body class='p-3 mb-2 bg-dark text-white'>\r\n"
				"<h2>Castle -- Automation</h2>\r\n";
LPSTR lpszHtmlBodyEnd = 	"</body>\r\n"
				"</html>";



typedef struct __ITERATE_TO_HTML_ARGS
{
	LPREQUEST_DATA		lprdReq;
	LPSTR 			lpszClass;
	LPSTR 			lpszName;
	LPVOID 			lpData;
	LPVOID 			lpOut;
} ITERATE_TO_HTML_ARGS, *LPITERATOR_TO_HTML_ARGS;




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE 
__AutomatonRestRoot(
	_In_ 		HANDLE 			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(lpUserData);
	
	LPSTR lpszTableStart;
	LPSTR lpszTableEnd;
	UARCHLONG ualIndex;
	HVECTOR hvRegistries;

	if (HTTP_METHOD_GET != hmType)
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	lpszTableStart = 	"<table style='width:100%' class='table table-striped table-dark'>\r\n"
				"<tr>\r\n"
				"<th>Registry</th>\r\n"
				"<th>Number Of Devices</th>\r\n"
				"</tr>\r\n";
	lpszTableEnd = 		"</table>\r\n";
	
	hvRegistries = AutomatonGetAllRegistries();
	if (NULLPTR == hvRegistries)
	{ return HTTP_STATUS_500_INTERNAL_SERVER_ERROR; }

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");

	RequestAddResponse(lprdData->hRequest, lpszHtmlHeadStart, StringLength(lpszHtmlHeadStart));
	RequestAddResponse(lprdData->hRequest, lpszHtmlHeadEnd, StringLength(lpszHtmlHeadEnd));
	RequestAddResponse(lprdData->hRequest, lpszHtmlBodyStart, StringLength(lpszHtmlBodyStart));

	RequestAddResponse(lprdData->hRequest, lpszTableStart, StringLength(lpszTableStart));

	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvRegistries);
		ualIndex++
	){
		HANDLE hRegistry;
		LPSTR lpszRegistryName;
		CSTRING csBuffer[512];

		ZeroMemory(csBuffer, sizeof(csBuffer));

		hRegistry = *(LPHANDLE)VectorAt(hvRegistries, ualIndex);
		if (NULL_OBJECT == hRegistry)
		{ continue; }

		lpszRegistryName = (LPSTR)AutomatonGetRegistryName(hRegistry);

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 0x01U,
			"<tr>\r\n"
			"<td><a href='/Automaton?Registry=%s' class='btn btn-secondary'>%s</a></td>\r\n"
			"<td>%lu</td>\r\n"
			"</tr>\r\n",
			lpszRegistryName,
			lpszRegistryName,
			AutomatonRegistryGetDeviceCount(hRegistry)
		);

		RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));
		FreeMemory(lpszRegistryName);
	}

	RequestAddResponse(lprdData->hRequest, lpszTableEnd, StringLength(lpszTableEnd));
	RequestAddResponse(lprdData->hRequest, lpszHtmlBodyEnd, StringLength(lpszHtmlBodyEnd));

	DestroyVector(hvRegistries);
	return HTTP_STATUS_200_OK;
}
				



_Success_ (return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__AutomatonIterateToHtmlResponse(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);

	HANDLE hAutomaton;
	LPITERATOR_TO_HTML_ARGS lpihaData;
	CSTRING csBuffer[1024];
	LPSTR lpszClass;
	LPSTR lpszName;
	BOOL bDo;

	hAutomaton = *(LPHANDLE)lpData;
	lpihaData = lpUserData;
	lpszClass = (LPSTR)AutomatonGetClass(hAutomaton);
	lpszName = (LPSTR)AutomatonGetName(hAutomaton);
	bDo = FALSE;

	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (NULLPTR == lpihaData->lpszClass && NULLPTR == lpihaData->lpszName)
	{ bDo = TRUE; } 
	else if (NULLPTR != lpihaData->lpszClass && lpihaData->lpszName)
	{
		if (StringInString(lpszClass, lpihaData->lpszClass))
		{ 
			if (StringInString(lpszName, lpihaData->lpszName))
			{ bDo = TRUE; }
		}
	}
	else if (NULLPTR != lpihaData->lpszClass && NULLPTR == lpihaData->lpszName)
	{
		if (StringInString(lpszClass, lpihaData->lpszClass))
		{ bDo = TRUE; }
	}
	else if (NULLPTR == lpihaData->lpszClass && NULLPTR != lpihaData->lpszName)
	{
		if (StringInString(lpszName, lpihaData->lpszName))
		{ bDo = TRUE; }
	}
	else 
	{ NO_OPERATION(); }

	if (TRUE == bDo)
	{
		HANDLE hRegistry;
		LPSTR lpszLight;
		LPSTR lpszRegistry;

		lpszLight = AutomatonToString(hAutomaton, NULLPTR, NULLPTR, AUTOMATON_TOSTRING_DESCRIPTION_LIGHT);
		hRegistry = AutomatonGetRegistry(hAutomaton);
		lpszRegistry = (LPSTR)AutomatonGetRegistryName(hRegistry);

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 0x01U,
			"<tr>\r\n"
			"<td><a href='/Automaton/Device?Handle=%p' class='btn btn-secondary'>%p</a></td>\r\n"
			"<td><a href='/Automaton?Class=%s' class='btn btn-secondary'>%s</a></td>\r\n"
			"<td><a href='/Automaton/Device?Registry=%s&Class=%s&Name=%s' class='btn btn-secondary'>%s</a></td>\r\n"
			"<td>%s</td>\r\n"
			"</tr>\r\n",
			hAutomaton,
			hAutomaton,
			lpszClass,
			lpszClass,
			lpszRegistry,
			lpszClass,
			lpszName,
			lpszName,
			lpszLight
		);

		FreeMemory(lpszLight);
		FreeMemory(lpszRegistry);
	}

	RequestAddResponse(lpihaData->lprdReq->hRequest, csBuffer, StringLength(csBuffer));
	return 0;
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE 
__AutomatonRestAutomaton(
	_In_ 		HANDLE 			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hRegistry;
	LPSTR lpszTableStart;
	LPSTR lpszTableEnd;
	LPSTR lpszRegistry;
	ITERATE_TO_HTML_ARGS ihaData;
	UARCHLONG ualIndex;

	if (HTTP_METHOD_GET != hmType)
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	lpszTableStart = 	"<table style='width:100%' class='table table-striped table-dark'>\r\n"
				"<tr>\r\n"
				"<th>HANDLE</th>\r\n"
				"<th>Device Class</th>\r\n"
				"<th>Device Name</th>\r\n"
				"<th>Description</th>\r\n"
				"</tr>\r\n";
	lpszTableEnd = 		"</table>\r\n";

	lpszRegistry = NULLPTR;

	ZeroMemory(&ihaData, sizeof(ihaData));
	ihaData.lprdReq = lprdData;

	ProcessQueryStringParameters(lprdData->hRequest);

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvQueryParameters);
		ualIndex++
	){
		LPQUERYSTRING_DATA lpqsdInfo;
		lpqsdInfo = VectorAt(lprdData->hvQueryParameters, ualIndex);

		if (0 == StringCompare("Class", lpqsdInfo->lpszKey))
		{ ihaData.lpszClass = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Name", lpqsdInfo->lpszKey))
		{ ihaData.lpszName = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Registry", lpqsdInfo->lpszKey))
		{ lpszRegistry = lpqsdInfo->lpszValue; }
	}

	hRegistry = AutomatonGetRegistryByName((LPCSTR)lpszRegistry);
	if (NULL_OBJECT == hRegistry)
	{ return HTTP_STATUS_500_INTERNAL_SERVER_ERROR; }

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");

	RequestAddResponse(lprdData->hRequest, lpszHtmlHeadStart, StringLength(lpszHtmlHeadStart));
	RequestAddResponse(lprdData->hRequest, lpszHtmlHeadEnd, StringLength(lpszHtmlHeadEnd));
	RequestAddResponse(lprdData->hRequest, lpszHtmlBodyStart, StringLength(lpszHtmlBodyStart));

	RequestAddResponse(lprdData->hRequest, lpszTableStart, StringLength(lpszTableStart));

	AutomatonRegistryTraverse(
		hRegistry,
		__AutomatonIterateToHtmlResponse,
		&ihaData
	);

	RequestAddResponse(lprdData->hRequest, lpszTableEnd, StringLength(lpszTableEnd));
	RequestAddResponse(lprdData->hRequest, lpszHtmlBodyEnd, StringLength(lpszHtmlBodyEnd));

	return HTTP_STATUS_200_OK;
}




_Success_ (return == NULLPTR, _Non_Locking_)
INTERNAL_OPERATION
LPVOID 
__AutomatonSearchForClassAndName(
	_In_ 		HBINARYSEARCHTREE	hbstData,
	_In_ 		LPVOID 			lpNode, 
	_In_ 		LPVOID 			lpData, 
	_In_Opt_ 	LPVOID 			lpUserData
){
	UNREFERENCED_PARAMETER(hbstData);
	UNREFERENCED_PARAMETER(lpNode);

	HANDLE hAutomaton;
	LPITERATOR_TO_HTML_ARGS lpihaData;
	LPSTR lpszClass;
	LPSTR lpszName;

	hAutomaton = *(LPHANDLE)lpData;
	lpihaData = lpUserData;
	lpszClass = (LPSTR)AutomatonGetClass(hAutomaton);
	lpszName = (LPSTR)AutomatonGetName(hAutomaton);

	if (0 == StringCompare(lpszClass, lpihaData->lpszClass))
	{
		if (0 == StringCompare(lpszName, lpihaData->lpszName))
		{ lpihaData->lpOut = hAutomaton; }
	}

	return NULLPTR;
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE 
__AutomatonRestDevice(
	_In_ 		HANDLE 			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hAutomaton;
	HANDLE hRegistry;
	HANDLE hRegistryToSearch;
	UARCHLONG ualIndex;
	LPSTR lpszTableStart;
	LPSTR lpszTableEnd;
	LPSTR lpszRegistry;
	LPSTR lpszRegistryToSearch;
	CSTRING csBuffer[1024];
	LPSTR lpszData;
	ITERATE_TO_HTML_ARGS ihaData;

	if (HTTP_METHOD_GET != hmType)
	{ return HTTP_STATUS_400_BAD_REQUEST; }
	
	ProcessQueryStringParameters(lprdData->hRequest);

	if (0 == VectorSize(lprdData->hvQueryParameters))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	lpszTableStart = 	"<table style='width:100%' class='table table-striped table-dark'>\r\n"
				"<tr>\r\n"
				"<th>Data</th>\r\n"
				"<th>Value</th>\r\n"
				"</tr>\r\n";
	lpszTableEnd = 		"</table>\r\n";

	ZeroMemory(&ihaData, sizeof(ihaData));
	ihaData.lprdReq = lprdData;
	hAutomaton = NULL_OBJECT;

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvQueryParameters);
		ualIndex++
	){
		LPQUERYSTRING_DATA lpqsdInfo;
		lpqsdInfo = VectorAt(lprdData->hvQueryParameters, ualIndex);

		if (0 == StringCompare("Handle", lpqsdInfo->lpszKey))
		{ 
			StringScanFormat(
				lpqsdInfo->lpszValue,
				"%p",
				&hAutomaton
			);
			
			if (NULL_OBJECT == hAutomaton)
			{ return HTTP_STATUS_400_BAD_REQUEST; }
		}
		if (0 == StringCompare("Class", lpqsdInfo->lpszKey))
		{ ihaData.lpszClass = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Name", lpqsdInfo->lpszKey))
		{ ihaData.lpszName = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Registry", lpqsdInfo->lpszKey))
		{ lpszRegistryToSearch = lpqsdInfo->lpszValue; }

	}

	

	if (NULL_OBJECT == hAutomaton)
	{ 
		hRegistryToSearch = AutomatonGetRegistryByName((LPCSTR)lpszRegistryToSearch);

		AutomatonRegistryTraverse(
			hRegistryToSearch,
			__AutomatonSearchForClassAndName,
			&ihaData
		);

		hAutomaton = ihaData.lpOut;
	}
	
	if (HANDLE_TYPE_AUTOMATON != GetHandleDerivativeType(hAutomaton))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/html");

	RequestAddResponse(lprdData->hRequest, lpszHtmlHeadStart, StringLength(lpszHtmlHeadStart));
	RequestAddResponse(lprdData->hRequest, lpszHtmlHeadEnd, StringLength(lpszHtmlHeadEnd));
	RequestAddResponse(lprdData->hRequest, lpszHtmlBodyStart, StringLength(lpszHtmlBodyStart));

	RequestAddResponse(lprdData->hRequest, lpszTableStart, StringLength(lpszTableStart));

	hRegistry = AutomatonGetRegistry(hAutomaton);
	lpszRegistry = (LPSTR)AutomatonGetRegistryName(hRegistry);

	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 0x01U,
		"<tr>\r\n"
		"<td>Handle</td>\r\n"
		"<td>%p</td>\r\n"
		"</tr>\r\n"
		"<tr>\r\n"
		"<td>Registry</td>\r\n"
		"<td>%s</td>\r\n"
		"</tr>\r\n"
		"<tr>\r\n"
		"<td>Device Class</td>\r\n"
		"<td>%s</td>\r\n"
		"</tr>\r\n"
		"<tr>\r\n"
		"<td>Device Name</td>\r\n"
		"<td>%s</td>\r\n"
		"</tr>\r\n"
		"<tr>\r\n"
		"<td>Description</td>\r\n"
		"<td>",
		hAutomaton,
		lpszRegistry,
		AutomatonGetClass(hAutomaton),
		AutomatonGetName(hAutomaton)
	);
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));
	FreeMemory(lpszRegistry);

	ZeroMemory(csBuffer, sizeof(csBuffer));
	lpszData = AutomatonToString(hAutomaton, NULLPTR, NULLPTR, AUTOMATON_TOSTRING_DESCRIPTION);
	RequestAddResponse(lprdData->hRequest, lpszData, StringLength(lpszData));
	FreeMemory(lpszData);
	
	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 0x01U,
		"</td>\r\n"
		"</tr>\r\n"
	);
	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));


	RequestAddResponse(lprdData->hRequest, lpszTableEnd, StringLength(lpszTableEnd));
	RequestAddResponse(lprdData->hRequest, lpszHtmlBodyEnd, StringLength(lpszHtmlBodyEnd));

	return HTTP_STATUS_200_OK;
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE 
__AutomatonRestDeviceOperation(
	_In_ 		HANDLE 			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT		lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(lpUserData);

	HANDLE hAutomaton;
	HANDLE hRegistryToSearch;
	LPSTR lpszTableStart;
	LPSTR lpszTableEnd;
	LPSTR lpszRegistryToSearch;
	CSTRING csBuffer[1024];
	LPSTR lpszData;
	UARCHLONG ualIndex;
	LPSTR lpszClass;
	LPSTR lpszName;
	LPSTR lpszOperation;
	LPVOID lpRet;
	HTTP_STATUS_CODE hscRet;

	if (HTTP_METHOD_GET != hmType)
	{ return HTTP_STATUS_400_BAD_REQUEST; }
	
	ProcessQueryStringParameters(lprdData->hRequest);

	if (0 == VectorSize(lprdData->hvQueryParameters))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	lpszTableStart = 	"<table style='width:100%' class='table table-striped table-dark'>\r\n"
				"<tr>\r\n"
				"<th>Data</th>\r\n"
				"<th>Value</th>\r\n"
				"</tr>\r\n";
	lpszTableEnd = 		"</table>\r\n";

	hAutomaton = NULL_OBJECT;

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lprdData->hvQueryParameters);
		ualIndex++
	){
		LPQUERYSTRING_DATA lpqsdInfo;
		lpqsdInfo = VectorAt(lprdData->hvQueryParameters, ualIndex);

		if (0 == StringCompare("Handle", lpqsdInfo->lpszKey))
		{ 
			StringScanFormat(
				lpqsdInfo->lpszValue,
				"%p",
				&hAutomaton
			);
			
			if (NULL_OBJECT == hAutomaton)
			{ return HTTP_STATUS_400_BAD_REQUEST; }
		}
		if (0 == StringCompare("Class", lpqsdInfo->lpszKey))
		{ lpszClass = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Name", lpqsdInfo->lpszKey))
		{ lpszName = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Registry", lpqsdInfo->lpszKey))
		{ lpszRegistryToSearch = lpqsdInfo->lpszValue; }
		else if (0 == StringCompare("Operation", lpqsdInfo->lpszKey))
		{ lpszOperation = lpqsdInfo->lpszValue; }
	}

	

	if (NULL_OBJECT == hAutomaton)
	{ 
		hRegistryToSearch = AutomatonGetRegistryByName((LPCSTR)lpszRegistryToSearch);
		hAutomaton = AutomatonRegistrySearch(
			hRegistryToSearch,
			(LPCSTR)lpszClass,
			(LPCSTR)lpszName
		);
	}
	
	if (HANDLE_TYPE_AUTOMATON != GetHandleDerivativeType(hAutomaton))
	{ return HTTP_STATUS_400_BAD_REQUEST; }

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain");

	/* Handle The Output */
	lpRet = AutomatonWriteEx(
		hAutomaton,
		lpszOperation,
		StringLength(lpszOperation),
		TYPE_LPSTR,
		NULLPTR,
		AUTOMATON_WRITE_DEVICE_OPERATION
	);

	ZeroMemory(csBuffer, sizeof(csBuffer));

	/* Success */
	if ((LPVOID)-1 != lpRet)
	{ 
		StringCopySafe(csBuffer, "Success", sizeof(csBuffer) - 0x01U); 
		hscRet = HTTP_STATUS_200_OK;
	}
	/* Failure */
	else 
	{ 
		StringCopySafe(csBuffer, "Failure", sizeof(csBuffer) - 0x01U); 
		hscRet = HTTP_STATUS_400_BAD_REQUEST;
	}

	RequestAddResponse(lprdData->hRequest, csBuffer, StringLength(csBuffer));
	return hscRet;
}




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
AutomatonRegisterRestEndpoints(
	_In_ 		HANDLE 			hHttpServer,
	_In_Opt_ 	LPVOID 			lpUserData
){
	EXIT_IF_UNLIKELY_NULL(hHttpServer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hHttpServer), HANDLE_TYPE_HTTP_SERVER, FALSE);

	BOOL bRet;
	bRet = TRUE;

	bRet &= DefineHttpUriProc(
		hHttpServer,
		"/",
		HTTP_METHOD_GET,
		__AutomatonRestRoot,
		lpUserData
	);

	bRet &= DefineHttpUriProc(
		hHttpServer,
		"/Automaton",
		HTTP_METHOD_GET,
		__AutomatonRestAutomaton,
		lpUserData
	);
	
	bRet &= DefineHttpUriProc(
		hHttpServer,
		"/Automaton/Device",
		HTTP_METHOD_GET,
		__AutomatonRestDevice,
		lpUserData
	);

	bRet &= DefineHttpUriProc(
		hHttpServer,
		"/Automaton/DeviceOperation",
		HTTP_METHOD_GET,
		__AutomatonRestDeviceOperation,
		lpUserData
	);
	

	return bRet;
}
/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CASTLE_SWITCHABLEDEVICEAUTOMATON_C
#define CASTLE_SWITCHABLEDEVICEAUTOMATON_C


#include "SwitchableDeviceAutomaton.h"
#include "AutomatonObject.c"




typedef struct __SWITCHABLE_DEVICE_AUTOMATON
{
	INHERITS_FROM_AUTOMATON(); 
	LPVOID 		lpChild;		/* Allow this to be inherited from */
	LPAUTOMATON	lpatDevice;
	LPSTR 		lpszOptions;
	USHORT 		usValue;

	DLPSTR 		dlpszOptionsSplit;	/* Created From StringSplit() */
	UARCHLONG 	ualNumberOfOptions;

	/* Created From StringSplit() */
	DLPSTR 		dlpszWritableQueues;
	UARCHLONG 	ualNumberOfWritableQueues;
	DLPSTR 		dlpszReadableQueues;
	UARCHLONG 	ualNumberOfReadableQueues;
} SWITCHABLE_DEVICE_AUTOMATON, *LPSWITCHABLE_DEVICE_AUTOMATON;




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceOnCreate(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_CREATE), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lpsdaDevice = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceOnDestroy(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_DESTROY), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lpsdaDevice = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceOnRead(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_READ), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lpsdaDevice = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceOnWrite(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, (LPVOID)-1);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, (LPVOID)-1);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, (LPVOID)-1);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_WRITE), 0, (LPVOID)-1);

	LPAUTOMATON lpatData;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	CSTRING csData[0x100];
	LPVOID lpRet;
	USHORT usValue;
	HANDLE hPodMQ;
	QUEUE_REQUEST qrData;
	UARCHLONG ualIndex;
	BOOL bDispatchToPodMQ;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, (LPVOID)-1);
	lpsdaDevice = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, (LPVOID)-1);

	ZeroMemory(csData, sizeof(csData));
	ZeroMemory(&qrData, sizeof(qrData));
	bDispatchToPodMQ = FALSE;
	lpRet = NULLPTR;

	if (ullFlags & AUTOMATON_WRITE_DEVICE_OPERATION)
	{
		EXIT_IF_UNLIKELY_NULL(lpData, (LPVOID)-1);
		EXIT_IF_UNLIKELY_NULL(ualBufferSize, (LPVOID)-1);
		EXIT_IF_UNLIKELY_NOT_VALUE(tyDataType, TYPE_LPSTR, (LPVOID)-1)

		LPSTR lpszOperation;
		CSTRING csBuffer[1024]; 

		ZeroMemory(csBuffer, sizeof(csBuffer));
		lpszOperation = lpData;

		if (0 == StringCompare(lpszOperation, "ON"))
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"Turning Device %p On!\n",
				lpatData->hThis
			);

			lpsdaDevice->usValue = MAX_USHORT;
			bDispatchToPodMQ = TRUE;
		}
		else if (0 == StringCompare(lpszOperation, "OFF"))
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"Turning Device %p Off!\n",
				lpatData->hThis
			);
			
			lpsdaDevice->usValue = 0;
			bDispatchToPodMQ = TRUE;
		}
		else if (0 != StringScanFormat(lpszOperation, "%hu", &usValue))
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"Turning Device %p to level %hu!\n",
				lpatData->hThis,
				usValue
			);

			lpsdaDevice->usValue = usValue;
			bDispatchToPodMQ = TRUE;
		}
		else
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"AutomatonWrite Error! No idea what following operation is: %s\n",
				lpszOperation
			);
			
			lpRet = (LPVOID)-1;
		}	

		/* PodMQ Out */
		StringPrintFormatSafe(
			csData,
			sizeof(csData) - 0x01U,
			"%s;%hu",
			lpsdaDevice->dlpszOptionsSplit[0],
			lpsdaDevice->usValue
		);

		/* Write Message */
		WriteFile(GetStandardError(), csBuffer, 0);
	}

	if (TRUE == bDispatchToPodMQ)
	{
		for (
			ualIndex = 0;
			ualIndex < lpsdaDevice->ualNumberOfWritableQueues;
			ualIndex++
		){
			DLPSTR dlpszOptions;
			LPSTR lpszServer;
			LPSTR lpszPort;
			LPSTR lpszQueueName;
			LPSTR lpszV6;
			UARCHLONG ualCount;
			USHORT usPort;
			BOOL bV6;

			ZeroMemory(&qrData, sizeof(qrData));
			
			ualCount = StringSplit(lpsdaDevice->dlpszWritableQueues[ualIndex], "@", &dlpszOptions);
			if (4 != ualCount)
			{ 
				WriteFile(GetStandardError(), "Incorrect Option For PUBLIC_IP_CLIENT_AUTOMATON Writable Queues!\n", 0);
				WriteFile(GetStandardError(), "Format: <server>@<port>@<queue_name>@<use_ipv6_1_or_0\n", 0);
				DestroySplitString(dlpszOptions);
				continue;
			}

			lpszServer = dlpszOptions[0];
			lpszPort = dlpszOptions[1];
			lpszQueueName = dlpszOptions[2];
			lpszV6 = dlpszOptions[3];

			StringScanFormat(lpszPort, "%hu", &usPort);
			StringScanFormat(lpszV6, "%hhu", &bV6);

			hPodMQ = CreateMessageQueueConnection(
				(LPCSTR)lpszServer,
				usPort,
				FALSE
			);

			if (NULL_OBJECT == hPodMQ)
			{ 
				WriteFile(GetStandardError(), "Failed to connect to PodMQ Server: ", 0);
				WriteFile(GetStandardError(), lpszServer, 0);
				WriteFile(GetStandardError(), ":", 0);
				WriteFile(GetStandardError(), lpszPort, 0);
				WriteFile(GetStandardError(), ".\n", 0);
				DestroySplitString(dlpszOptions);
				continue;
			}

			StringCopySafe(qrData.csQueue, lpszQueueName, sizeof(qrData.csQueue) - 0x01U);
			qrData.qdcData.usDataType = QUEUE_DATA_TYPE_STRING;
			qrData.qdcData.lpData = csData;
			qrData.qdcData.ullDataSize = StringLength(csData);

			SendMessageQueueConnectionRequest(
				hPodMQ,
				PUSH_QUEUE,
				&qrData
			);

			DestroyObject(hPodMQ);
			DestroySplitString(dlpszOptions);
		}
	}

	return lpRet;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceOnTick(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TICK), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lpsdaDevice = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceToString(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TOSTRING), 0, NULLPTR);

	LPAUTOMATON lpamDevice;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	DLPVOID dlpOut;
	LPSTR lpszOut;
	CSTRING csBuffer[1024];
	
	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, NULLPTR);
	lpsdaDevice = lpamDevice->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, NULLPTR);

	dlpOut = (DLPVOID)lpData;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	/* Return New String */
	if (NULLPTR == dlpOut)
	{
		if (AUTOMATON_TOSTRING_DESCRIPTION == ullFlags)
		{
			LPSTR lpszRegistry;
			lpszRegistry = (LPSTR)AutomatonGetRegistryName(lpamDevice->hRegistry);

			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div>%s</div>\r\n"
				"<div><a href='javascript:DoXhrGet(\"/Automaton/DeviceOperation?Registry=%s&Class=%s&Name=%s&Operation=ON\");' class='btn btn-secondary'>On</a>\r\n"
				"<a href='javascript:DoXhrGet(\"/Automaton/DeviceOperation?Registry=%s&Class=%s&Name=%s&Operation=OFF\");' class='btn btn-secondary'>Off</a>\r\n"
				"| <b>Status:</b> %s</div>\r\n",
				lpsdaDevice->lpszDescription,
				lpszRegistry,
				lpamDevice->lpcszClass,
				lpamDevice->lpcszName,
				lpszRegistry,
				lpamDevice->lpcszClass,
				lpamDevice->lpcszName,
				(0 == lpsdaDevice->usValue) ? "OFF" : "ON"
			);

			lpszOut = StringDuplicate(csBuffer);
			FreeMemory(lpszRegistry);
			return lpszOut;
		}
		else if (AUTOMATON_TOSTRING_DESCRIPTION_LIGHT == ullFlags)
		{
			LONG lPercent;
			LPSTR lpszRegistry;

			lpszRegistry = (LPSTR)AutomatonGetRegistryName(lpamDevice->hRegistry);
			lPercent = (LONG)(100.0f * ((lpsdaDevice->usValue * 1.0f) / MAX_USHORT));

			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div class='btn-group' role='group' aria-label='Device Opertions'>%s \r\n"
				"<a href='javascript:DoXhrGet(\"/Automaton/DeviceOperation?Registry=%s&Class=%s&Name=%s&Operation=ON\");' class='btn btn-secondary'>On</a>\r\n"
				"<a href='javascript:DoXhrGet(\"/Automaton/DeviceOperation?Registry=%s&Class=%s&Name=%s&Operation=OFF\");' class='btn btn-secondary'>Off</a></div>\r\n"
				"<div class='progress'><div class='progress-bar' role='progressbar' style='width: %u%%' aria-valuenow='%u' aria-valuemin='0' aria-valuemax='65535'>%u%%</div></div>\r\n",
				lpsdaDevice->lpszDescription,
				lpszRegistry,
				lpamDevice->lpcszClass,
				lpamDevice->lpcszName,
				lpszRegistry,
				lpamDevice->lpcszClass,
				lpamDevice->lpcszName,
				(ULONG)lPercent,
				(ULONG)lpsdaDevice->usValue,
				(ULONG)lPercent
			);

			lpszOut = StringDuplicate(csBuffer);
			FreeMemory(lpszRegistry);
			return lpszOut;
		}
	}

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__SwitchableDeviceToJson(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TOJSON), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lpsdaDevice = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lpsdaDevice, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE
CreateSwitchableDeviceAutomaton(
	_In_ 		HANDLE 		hRegistry,
	_In_Z_ 		LPCSTR RESTRICT	lpcszDeviceName,
	_In_Opt_Z_	LPCSTR RESTRICT	lpcszDescription,
	_In_Z_ 		DLPCSTR		dlpcszQueuesToWriteTo,
	_In_ 		UARCHLONG	ualNumberOfWriteableQueues,
	_In_Z_ 		DLPCSTR		dlpcszQueuesToReadFrom,
	_In_ 		UARCHLONG	ualNumberOfReadableQueues,
	_In_Opt_Z_ 	LPCSTR 		lpcszOptions,
	_In_Opt_ 	LPVOID RESTRICT	lpChild,
	_In_Opt_	LPVOID RESTRICT	lpParam,
	_In_Opt_ 	ULONGLONG 	ullFlags
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszDeviceName, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszOptions, NULL_OBJECT);

	HANDLE hAutomaton;
	LPSWITCHABLE_DEVICE_AUTOMATON lpsdaDevice;
	
	lpsdaDevice = GlobalAllocAndZero(sizeof(SWITCHABLE_DEVICE_AUTOMATON));
	if (NULLPTR == lpsdaDevice)
	{ JUMP(__DEVICE_ALLOC_FAILED); }

	lpsdaDevice->lpszDescription = StringDuplicate(lpcszDescription);
	if (NULLPTR == lpsdaDevice->lpszDescription)
	{ JUMP(__CUST_DESC_FAILED); }

	lpsdaDevice->lpChild = lpChild;
	lpsdaDevice->dlpszWritableQueues = (DLPSTR)dlpcszQueuesToWriteTo;
	lpsdaDevice->ualNumberOfWritableQueues = ualNumberOfWriteableQueues;
	lpsdaDevice->dlpszReadableQueues = (DLPSTR)dlpcszQueuesToReadFrom;
	lpsdaDevice->ualNumberOfReadableQueues = ualNumberOfReadableQueues;
	lpsdaDevice->lpszOptions = StringDuplicate((LPSTR)lpcszOptions);

	if (NULLPTR == lpsdaDevice->lpszOptions)
	{ JUMP(__OPTIONS_FAILED); }

	lpsdaDevice->ualNumberOfOptions = StringSplit(
		lpcszOptions,
		";",
		&(lpsdaDevice->dlpszOptionsSplit)
	);

	hAutomaton = CreateAutomatonEx(
		hRegistry,
		AUTOMATON_CLASS_SWITCHABLE_DEVICE,
		lpcszDeviceName,
		__SwitchableDeviceOnCreate,
		__SwitchableDeviceOnDestroy,
		__SwitchableDeviceOnRead,
		__SwitchableDeviceOnWrite,
		__SwitchableDeviceOnTick,
		__SwitchableDeviceToString,
		__SwitchableDeviceToJson,
		NULLPTR,
		NULLPTR,
		0,
		lpsdaDevice,
		lpParam,
		ullFlags
	);

	if (NULL_OBJECT == hAutomaton)
	{ JUMP(__OBJECT_FAILED); }

	lpsdaDevice->hAutomaton = hAutomaton;
	lpsdaDevice->lpatDevice = OBJECT_CAST(hAutomaton, LPAUTOMATON);

	return hAutomaton;

	__OBJECT_FAILED:
	FreeMemory(lpsdaDevice->lpszOptions);
	__OPTIONS_FAILED:
	__CUST_DESC_FAILED:
	FreeMemory(lpsdaDevice);
	__DEVICE_ALLOC_FAILED:
	return NULL_OBJECT;
}







#endif




/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "Automaton.h"
#include "AutomatonObject.c"




_Call_Back_
INTERNAL_OPERATION
BOOL 
__DestroyAutomaton(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	return TRUE;
}



_Call_Back_ 
INTERNAL_OPERATION
LPVOID 
__DefaultToString(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpcszEvent);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	
	LPAUTOMATON lpamDevice;
	DLPVOID dlpOut;
	LPSTR lpszOut;
	CSTRING csBuffer[1024];

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	dlpOut = (DLPVOID)lpData;
	ZeroMemory(csBuffer, sizeof(csBuffer));


	/* Return New String */
	if (NULLPTR == dlpOut)
	{
		if (AUTOMATON_TOSTRING_DESCRIPTION == ullFlags)
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div><b>%p</b>-%s:%s</div>\r\n"
				"<div>Lock Handle: %p</div>\r\n"
				"<div>Child Data: %p</div>\r\n"
				"<div>Construtor: %p</div>\r\n"
				"<div>Destructor: %p</div>\r\n"
				"<div>Read: %p</div>\r\n"
				"<div>Write: %p</div>\r\n"
				"<div>Tick: %p</div>\r\n"
				"<div>ToString: %p</div>\r\n"
				"<div>ToJson: %p</div>\r\n"
				"<div>Flags: %p</div>\r\n",
				lpamDevice,
				lpamDevice->lpcszClass,
				lpamDevice->lpcszName,
				lpamDevice->hLock,
				lpamDevice->lpChildData,
				lpamDevice->lpfnCreate,
				lpamDevice->lpfnDestroy,
				lpamDevice->lpfnRead,
				lpamDevice->lpfnWrite,
				lpamDevice->lpfnTick,
				lpamDevice->lpfnToString,
				lpamDevice->lpfnToJson,
				(LPVOID)lpamDevice->ullFlags
			);

			lpszOut = GlobalAllocAndZero(StringLength(csBuffer) + 0x01U);
			CopyMemory(lpszOut, csBuffer, StringLength(csBuffer));
			return lpszOut;
		}
	}

	return NULLPTR;
	
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE 
CreateAutomaton(
	_In_ 		HANDLE 				hRegistry,	/* Automaton Registry */
	_In_ 		LPCSTR 				lpcszClass,	/* Device Class */
	_In_ 		LPCSTR 				lpcszName,	/* Device Name */
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnCreate,	
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnDestroy,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnRead,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnWrite,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnTick,
	_In_ 		LPVOID 				lpChildData,
	_In_ 		LPVOID 				lpParam,	/* To Be Passed To All Callbacks */
	_In_ 		ULONGLONG 			ullFlags	/* Operation Flags */
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRegistry), HANDLE_TYPE_AUTOMATON_REGISTRY, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszClass, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszName, NULL_OBJECT);
	/*
	EXIT_IF_UNLIKELY_NULL(lpfnCreate, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpfnDestroy, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpfnRead, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpfnWrite, NULL_OBJECT);
	*/

	HANDLE hAutomaton;
	LPAUTOMATON lpamDevice;
	UARCHLONG ualLength;
	
	lpamDevice = GlobalAllocAndZero(sizeof(AUTOMATON));
	if (NULLPTR == lpamDevice)
	{ JUMP(__AUTOMATON_ALLOC_FAILED); }

	lpamDevice->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lpamDevice->hLock)
	{ JUMP(__AUTOMATON_CRIT_SEC_FAILED); }

	lpamDevice->hhtRegisteredOperations = CreateHashTable(0x2000);
	if (NULLPTR == lpamDevice->hhtRegisteredOperations)
	{ JUMP(__AUTOMATON_HASHTABLE_FAILED); }

	ualLength = StringLength(lpcszClass);
	lpamDevice->lpcszClass = GlobalAllocAndZero(ualLength + 0x01U);
	if (NULLPTR == lpamDevice->lpcszClass)
	{ JUMP(__AUTOMATON_CLASS_FAILED); }
	StringCopySafe(
		(LPSTR)lpamDevice->lpcszClass,
		lpcszClass,
		ualLength
	);

	ualLength = StringLength(lpcszName);
	lpamDevice->lpcszName = GlobalAllocAndZero(ualLength + 0x01U);
	if (NULLPTR == lpamDevice->lpcszName)
	{ JUMP(__AUTOMATON_NAME_FAILED); }
	StringCopySafe(
		(LPSTR)lpamDevice->lpcszName,
		lpcszName,
		ualLength
	);

	lpamDevice->lpChildData = lpChildData;
	lpamDevice->lpData = lpParam;
	lpamDevice->ullFlags = ullFlags;
	lpamDevice->hRegistry = hRegistry;
	lpamDevice->lpfnCreate = lpfnCreate;
	lpamDevice->lpfnDestroy = lpfnDestroy;
	lpamDevice->lpfnRead = lpfnRead;
	lpamDevice->lpfnWrite = lpfnWrite;
	lpamDevice->lpfnTick = lpfnTick;
	lpamDevice->lpfnToString = __DefaultToString;

	hAutomaton = CreateHandleWithSingleInheritor(
		lpamDevice,
		&(lpamDevice->hThis),
		HANDLE_TYPE_AUTOMATON,
		__DestroyAutomaton,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpamDevice->ullId),
		0
	);

	if (NULL_OBJECT == hAutomaton)
	{ JUMP(__AUTOMATON_OBJECT_FAILED); }

	AutomatonRegistryAdd(
		hRegistry,
		hAutomaton
	);

	if (!(ullFlags & AUTOMATON_CREATION_DEFERRED))
	{
		lpamDevice->lpfnCreate(
			hAutomaton,
			AUTOMATON_EVENT_CREATE,
			NULLPTR,
			0,
			NULLPTR,
			255,
			NULLPTR,
			lpParam,
			ullFlags
		);
	}

	return hAutomaton;
	
	/* Failure Section */
	__AUTOMATON_OBJECT_FAILED:
	FreeMemory((LPVOID)lpamDevice->lpcszName);
	__AUTOMATON_NAME_FAILED:
	FreeMemory((LPVOID)lpamDevice->lpcszClass);
	__AUTOMATON_CLASS_FAILED:
	DestroyHashTable(lpamDevice->hhtRegisteredOperations);
	__AUTOMATON_HASHTABLE_FAILED:
	DestroyObject(lpamDevice->hLock);
	__AUTOMATON_CRIT_SEC_FAILED:
	FreeMemory(lpamDevice);
	__AUTOMATON_ALLOC_FAILED:
	return (LPVOID)(UARCHLONG)-1;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE 
CreateAutomatonEx(
	_In_ 		HANDLE 				hRegistry,			/* Automaton Registry */
	_In_ 		LPCSTR 				lpcszClass,			/* Device Class */
	_In_ 		LPCSTR 				lpcszName,			/* Device Name */
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnCreate,	
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnDestroy,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnRead,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnWrite,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnTick,
	_In_Opt_	LPFN_AUTOMATON_OPERATION	lpfnToString,
	_In_Opt_	LPFN_AUTOMATON_OPERATION	lpfnToJson,
	_In_Opt_ 	DLPFN_AUTOMATON_OPERATION	dlpfnOperations,
	_In_Opt_Z_ 	DLPCSTR 			dlpcszEventsForOperations,
	_In_Opt_ 	UARCHLONG 			ualNumberOfExtraEvents,
	_In_ 		LPVOID 				lpChildData,
	_In_ 		LPVOID 				lpParam,			/* To Be Passed To All Callbacks */
	_In_ 		ULONGLONG 			ullFlags			/* Operation Flags */
){
	HANDLE hAutomaton;
	LPAUTOMATON lpamDevice;

	hAutomaton = CreateAutomaton(
		hRegistry,
		lpcszClass,
		lpcszName,
		lpfnCreate,
		lpfnDestroy,
		lpfnRead,
		lpfnWrite,
		lpfnTick,
		lpChildData,
		lpParam,
		ullFlags | AUTOMATON_CREATION_DEFERRED
	);

	if (NULL_OBJECT == hAutomaton)
	{ return NULL_OBJECT; }

	lpamDevice = OBJECT_CAST(hAutomaton, LPAUTOMATON);
	if (NULLPTR == lpamDevice)
	{
		DestroyObject(hAutomaton);
		return NULL_OBJECT;
	}

	if (NULLPTR != lpfnToString)
	{ lpamDevice->lpfnToString = lpfnToString; }
	lpamDevice->lpfnToJson = lpfnToJson;

	if (NULLPTR != dlpfnOperations && NULLPTR != dlpcszEventsForOperations && 0 != ualNumberOfExtraEvents)
	{
		UARCHLONG ualIndex;
		for (
			ualIndex = 0;
			ualIndex < ualNumberOfExtraEvents;
			ualIndex++
		){
			AutomatonRegisterOperation(
				hAutomaton,
				dlpcszEventsForOperations[ualIndex],
				dlpfnOperations[ualIndex]
			);
		}
	}

	if (!(ullFlags & AUTOMATON_CREATION_DEFERRED))
	{
		lpamDevice->lpfnCreate(
			hAutomaton,
			AUTOMATON_EVENT_CREATE,
			NULLPTR,
			0,
			NULLPTR,
			255,
			NULLPTR,
			lpParam,
			ullFlags
		);
	}

	return hAutomaton;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
CASTLE_API
LPVOID
AutomatonGetData(
	_In_ 		HANDLE 				hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);

	LPAUTOMATON lpamDevice;
	LPVOID lpRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, NULLPTR);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	lpRet = lpamDevice->lpData;
	ReleaseSingleObject(lpamDevice->hLock);

	return lpRet;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL
AutomatonSetData(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPVOID 				lpData
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	lpamDevice->lpData = lpData;
	ReleaseSingleObject(lpamDevice->hLock);

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegisterOperation(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPCSTR 				lpcszEvent,
	_In_		LPFN_AUTOMATON_OPERATION	lpfnOperation
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;
	BOOL bRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);

	bRet = HashTableInsertEx(
		lpamDevice->hhtRegisteredOperations,
		(ULONGLONG)lpcszEvent,
		StringLength(lpcszEvent),
		FALSE,
		(ULONGLONG)lpfnOperation
	);

	ReleaseSingleObject(lpamDevice->hLock);

	return bRet;
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID 
AutomatonRead(
	_In_ 		HANDLE 				hObject,
	_Out_ 		LPVOID 				lpOut,
	_In_ 		UARCHLONG 			ualOutBufferSize,
	_In_Opt_ 	LPVOID 				lpParam
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_READ,
		lpOut,
		ualOutBufferSize,
		NULLPTR,
		255,
		NULLPTR,
		lpParam,
		0
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID 
AutomatonReadEx(
	_In_ 		HANDLE 				hObject,
	_Out_ 		LPVOID 				lpOut,
	_In_ 		UARCHLONG 			ualOutBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_READ,
		lpOut,
		ualOutBufferSize,
		lpualBytesWritten,
		255, 
		lptyOutputType,
		lpParam,
		ullFlags
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID
AutomatonWrite(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize,
	_In_Opt_ 	LPVOID 				lpParam
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_WRITE,
		lpData,
		ualDataSize,
		NULLPTR,
		255,
		NULLPTR,
		lpParam,
		0
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID
AutomatonWriteEx(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize,
	_In_Opt_ 	TYPE 				tyData,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_WRITE,
		lpData,
		ualDataSize,
		NULLPTR,
		tyData,
		NULLPTR,
		lpParam,
		ullFlags
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation)
CASTLE_API
LPVOID
AutomatonTick(
	_In_ 		HANDLE 				hObject,
	_In_Opt_	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_TICK,
		NULLPTR,
		0,
		NULLPTR,
		0xFF,
		NULLPTR,
		lpParam,
		ullFlags
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API
LPVOID 
AutomatonToString(
	_In_ 		HANDLE 				hObject,
	_Out_ 		DLPVOID 			dlpOut,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_TOSTRING,
		(LPVOID)dlpOut,
		0,
		NULLPTR,
		0xFF,
		NULLPTR,
		lpParam,
		ullFlags
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API
LPVOID 
AutomatonToJson(
	_In_ 		HANDLE 				hObject,
	_Out_ 		DLPVOID 			dlpOut,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
){
	return AutomatonExec(
		hObject,
		AUTOMATON_EVENT_TOJSON,
		(LPVOID)dlpOut,
		0,
		NULLPTR,
		0xFF,
		NULLPTR,
		lpParam,
		ullFlags
	);
}




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID
AutomatonExec(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	EXIT_IF_UNLIKELY_NULL(hObject, (LPVOID)(UARCHLONG)-1);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, (LPVOID)(UARCHLONG)-1);

	LPAUTOMATON lpamDevice;
	LPVOID lpRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	
	/* Handle The Event */
	if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_CREATE))
	{
		lpRet = lpamDevice->lpfnCreate(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	else if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_DESTROY))
	{
		lpRet = lpamDevice->lpfnDestroy(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	else if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_READ))
	{
		lpRet = lpamDevice->lpfnRead(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	else if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_WRITE))
	{
		lpRet = lpamDevice->lpfnWrite(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	else if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_TICK))
	{
		lpRet = lpamDevice->lpfnTick(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	else if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_TOSTRING))
	{
		lpRet = lpamDevice->lpfnToString(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	else if (0 == StringCompare(lpcszEvent, AUTOMATON_EVENT_TOJSON))
	{
		lpRet = lpamDevice->lpfnToString(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	/* HashTable Lookup On Event */
	else
	{ 
		LPFN_AUTOMATON_OPERATION lpfnCall;
		UARCHLONG ualKey;
	
		ualKey = HashTableGetKey(
			lpamDevice->hhtRegisteredOperations,
			(ULONGLONG)lpcszEvent,
			StringLength(lpcszEvent),
			FALSE
		);

		if ((UARCHLONG)-1 == ualKey)
		{ 
			lpRet = (LPVOID)(UARCHLONG)-1;
			JUMP(__AUTOMATON_EXEC_EXIT);
		}

		HashTableGetValueEx(
			lpamDevice->hhtRegisteredOperations,
			ualKey,
			NULLPTR,
			NULLPTR,
			(LPULONGLONG)&lpfnCall
		);

		if (NULLPTR == lpfnCall)
		{ 
			lpRet = (LPVOID)(UARCHLONG)-1; 
			JUMP(__AUTOMATON_EXEC_EXIT);
		}

		lpRet = lpfnCall(
			hObject,
			lpcszEvent,
			lpData,
			ualBufferSize,
			lpualBytesWritten,
			tyDataType,
			lptyOutputType,
			lpParam,
			ullFlags
		);
	}
	
	__AUTOMATON_EXEC_EXIT:
	ReleaseSingleObject(lpamDevice->hLock);
	return lpRet;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
CASTLE_API 
LPCSTR 
AutomatonGetName(
	_In_ 		HANDLE 				hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;
	LPCSTR lpcszRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	lpcszRet = lpamDevice->lpcszName;
	ReleaseSingleObject(lpamDevice->hLock);

	return lpcszRet;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
CASTLE_API 
LPCSTR 
AutomatonGetClass(
	_In_ 		HANDLE 				hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;
	LPCSTR lpcszRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	lpcszRet = lpamDevice->lpcszClass;
	ReleaseSingleObject(lpamDevice->hLock);

	return lpcszRet;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API 
BOOL 
AutomatonTriggeredRead(
	_In_ 		HANDLE 				hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;
	BOOL bRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	bRet = lpamDevice->bTriggeredRead;
	ReleaseSingleObject(lpamDevice->hLock);

	return bRet;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API 
BOOL 
AutomatonTriggeredWrite(
	_In_ 		HANDLE 				hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;
	BOOL bRet;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	WaitForSingleObject(lpamDevice->hLock, INFINITE_WAIT_TIME);
	bRet = lpamDevice->bTriggeredWrite;
	ReleaseSingleObject(lpamDevice->hLock);

	return bRet;
}




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE 
AutomatonGetRegistry(
	_In_ 		HANDLE 				hObject
){
	EXIT_IF_UNLIKELY_NULL(hObject, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, FALSE);

	LPAUTOMATON lpamDevice;

	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, FALSE);

	return lpamDevice->hRegistry;
}
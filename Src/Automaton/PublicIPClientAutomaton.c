/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "PublicIPClientAutomaton.h"
#include "AutomatonObject.c"




typedef struct __PUBLIC_IP_AUTOMATON
{
	INHERITS_FROM_AUTOMATON();
	LPSTR 		lpszServer;
	LPSTR 		lpszUri;
	USHORT 		usPort;
	BOOL 		bHttps;
	BOOL 		bCertCheck;

	/* Created From StringSplit() */
	DLPCSTR 	dlpcszWritableQueues;
	UARCHLONG 	ualNumberOfWritableQueues;

	LPVOID 		lpParam;
	ULONGLONG	ullFlags;
} PUBLIC_IP_AUTOMATON, *LPPUBLIC_IP_AUTOMATON;




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpOnCreate(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_CREATE), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	lppipData->lpszDescription = StringDuplicate("0.0.0.0");

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpOnDestroy(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_DESTROY), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpOnRead(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_READ), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpOnWrite(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_WRITE), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	if (AUTOMATON_WRITE_TRIGGERED_BY_TICK & ullFlags)
	{
		HANDLE hPodMQ;
		QUEUE_REQUEST qrData;
		UARCHLONG ualIndex;

		DLPSTR dlpszOptions;
		UARCHLONG ualCount;
		LPSTR lpszServer;
		LPSTR lpszPort;
		LPSTR lpszQueueName;
		LPSTR lpszV6;
		USHORT usPort;
		BOOL bV6;

		/* Reset */
		lpatData->bTriggeredWrite = FALSE; 
		
		for (
			ualIndex = 0;
			ualIndex < lppipData->ualNumberOfWritableQueues;
			ualIndex++
		){
			ZeroMemory(&qrData, sizeof(qrData));
			
			ualCount = StringSplit(lppipData->dlpcszWritableQueues[ualIndex], "@", &dlpszOptions);
			if (4 != ualCount)
			{ 
				WriteFile(GetStandardError(), "Incorrect Option For PUBLIC_IP_CLIENT_AUTOMATON Writable Queues!\n", 0);
				WriteFile(GetStandardError(), "Format: <server>@<port>@<queue_name>@<use_ipv6_1_or_0\n", 0);
				DestroySplitString(dlpszOptions);
				continue;
			}

			lpszServer = dlpszOptions[0];
			lpszPort = dlpszOptions[1];
			lpszQueueName = dlpszOptions[2];
			lpszV6 = dlpszOptions[3];

			StringScanFormat(lpszPort, "%hu", &usPort);
			StringScanFormat(lpszV6, "%hhu", &bV6);

			hPodMQ = CreateMessageQueueConnection(
				(LPCSTR)lpszServer,
				usPort,
				FALSE
			);

			if (NULL_OBJECT == hPodMQ)
			{ 
				WriteFile(GetStandardError(), "Failed to connect to PodMQ Server: ", 0);
				WriteFile(GetStandardError(), lpszServer, 0);
				WriteFile(GetStandardError(), ":", 0);
				WriteFile(GetStandardError(), lpszPort, 0);
				WriteFile(GetStandardError(), ".\n", 0);
				DestroySplitString(dlpszOptions);
				continue;
			}

			StringCopySafe(qrData.csQueue, lpszQueueName, sizeof(qrData.csQueue) - 0x01U);
			qrData.qdcData.lpData = lppipData->lpszDescription;
			qrData.qdcData.ullDataSize = StringLength(lppipData->lpszDescription);
			qrData.qdcData.usDataType = QUEUE_DATA_TYPE_STRING;

			SendMessageQueueConnectionRequest(
				hPodMQ,
				PUSH_QUEUE,
				&qrData
			);

			DestroyObject(hPodMQ);
			DestroySplitString(dlpszOptions);
		}
	}

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpOnTick(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TICK), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	HANDLE hRequest;
	CSTRING csOutput[64];
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	ZeroMemory(csOutput, sizeof(csOutput));

	if (TRUE == lppipData->bHttps)
	{
		hRequest = CreateHttpsRequest(
			HTTP_REQUEST_GET,
			HTTP_VERSION_1_1,
			lppipData->lpszServer,
			lppipData->usPort,
			lppipData->lpszUri,
			HTTP_CONNECTION_CLOSE,
			"Castle Public IP Automaton/1.0",
			HTTP_ACCEPT_PLAIN_TEXT,
			FALSE,
			lppipData->bCertCheck
		);
		
		if (NULL_OBJECT == hRequest)
		{ return NULLPTR; }

		ExecuteHttpsRequest(
			hRequest,
			csOutput,
			sizeof(csOutput),
			NULLPTR
		);
	}
	else 
	{
		hRequest = CreateHttpRequest(
			HTTP_REQUEST_GET,
			HTTP_VERSION_1_1,
			lppipData->lpszServer,
			lppipData->usPort,
			lppipData->lpszUri,
			"Castle Public IP Automaton/1.0",
			HTTP_ACCEPT_PLAIN_TEXT,
			FALSE
		);

		if (NULL_OBJECT == hRequest)
		{ return NULLPTR; }
		
		ExecuteHttpRequest(
			hRequest,
			csOutput,
			sizeof(csOutput),
			NULLPTR
		);
	}

	if (NULLPTR != lppipData->lpszDescription)
	{ FreeMemory(lppipData->lpszDescription); }

	lppipData->lpszDescription = StringDuplicate((LPCSTR)csOutput);
	DestroyObject(hRequest);

	if (NULLPTR != lppipData->dlpcszWritableQueues)
	{ lpatData->bTriggeredWrite = TRUE; }

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpToString(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TOSTRING), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	DLPVOID dlpOut;
	LPSTR lpszOut;
	CSTRING csBuffer[1024];
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	dlpOut = (DLPVOID)lpData;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	/* Return New String */
	if (NULLPTR == dlpOut)
	{
		if (AUTOMATON_TOSTRING_DESCRIPTION == ullFlags)
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div>%s</div>\r\n",
				lppipData->lpszDescription
			);

			lpszOut = StringDuplicate((LPCSTR)csBuffer);
			return lpszOut;
		}
		else if (AUTOMATON_TOSTRING_DESCRIPTION_LIGHT == ullFlags)
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div>%s</div>\r\n",
				lppipData->lpszDescription
			);

			lpszOut = StringDuplicate((LPCSTR)csBuffer);
			return lpszOut;
		}
	}

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PublicIpToJson(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TOJSON), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPPUBLIC_IP_AUTOMATON lppipData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lppipData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lppipData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE
CreatePublicIPClientAutomaton(
	_In_ 		HANDLE 		hRegistry,
	_In_Z_ 		LPCSTR RESTRICT	lpcszDeviceName,
	_In_Z_ 		LPCSTR RESTRICT	lpcszServer,
	_In_Z_ 		LPCSTR RESTRICT	lpcszUri,
	_In_ 		BOOL 		bHttps,
	_In_ 		BOOL 		bCertCheck,
	_In_ 		USHORT 		usPort,
	_In_Z_ 		DLPCSTR		dlpcstrQueuesToWriteTo,
	_In_ 		UARCHLONG	ualNumberOfWriteableQueues,
	_In_ 		LPVOID 		lpParam,
	_In_Opt_ 	ULONGLONG 	ullFlags
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszDeviceName, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszServer, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszUri, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(usPort, NULL_OBJECT);

	HANDLE hAutomaton;
	LPPUBLIC_IP_AUTOMATON lppipData;

	lppipData = GlobalAllocAndZero(sizeof(PUBLIC_IP_AUTOMATON));
	if (NULLPTR == lppipData)
	{ JUMP(__CLASS_DATA_FAILED); }

	lppipData->bHttps = bHttps;
	lppipData->bCertCheck = bCertCheck;
	lppipData->usPort = usPort;
	lppipData->lpParam = lpParam;
	lppipData->ullFlags = ullFlags;
	lppipData->dlpcszWritableQueues = dlpcstrQueuesToWriteTo;
	lppipData->ualNumberOfWritableQueues = ualNumberOfWriteableQueues;

	lppipData->lpszServer = StringDuplicate(lpcszServer);
	if (NULLPTR == lppipData->lpszServer)
	{ JUMP(__SERVER_STR_FAILED); }
	
	lppipData->lpszUri = StringDuplicate(lpcszUri);
	if (NULLPTR == lppipData->lpszUri)
	{ JUMP(__URI_STR_FAILED); }

	hAutomaton = CreateAutomatonEx(
		hRegistry,
		AUTOMATON_CLASS_PUBLICIP_CLIENT,
		lpcszDeviceName,
		__PublicIpOnCreate,
		__PublicIpOnDestroy,
		__PublicIpOnRead,
		__PublicIpOnWrite,
		__PublicIpOnTick,
		__PublicIpToString,
		__PublicIpToJson,
		NULLPTR,
		NULLPTR,
		0,
		lppipData,
		lpParam,
		ullFlags
	);

	if (NULL_OBJECT == hAutomaton)
	{ JUMP (__OBJECT_FAILED); }

	lppipData->hAutomaton = hAutomaton;

	return hAutomaton;

	__OBJECT_FAILED:
	FreeMemory(lppipData->lpszUri);
	__URI_STR_FAILED:
	FreeMemory(lppipData->lpszServer);
	__SERVER_STR_FAILED:
	FreeMemory(lppipData);
	__CLASS_DATA_FAILED:
	return hAutomaton;	
}



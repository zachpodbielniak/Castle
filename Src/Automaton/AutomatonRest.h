/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CASTLE_AUTOMATONREST_H
#define CASTLE_AUTOMATONREST_H


#include "Automaton.h"
#include "AutomatonRegistry.h"




typedef struct __AUTOMATON_REST_PARAMS
{
	HANDLE 			hDefaultRegistry;
} AUTOMATON_REST_PARAMS, *LPAUTOMATON_REST_PARAMS;




_Success_(return != FALSE, _Non_Locking_)
CROCK_API
BOOL 
AutomatonRegisterRestEndpoints(
	_In_ 		HANDLE 			hHttpServer,
	_In_Opt_ 	LPVOID 			lpUserData
);




#endif

/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "PodMQClientAutomaton.h"
#include "AutomatonObject.c"


typedef struct __PODMQ_CLIENT_AUTOMATON
{
	INHERITS_FROM_AUTOMATON();
	HANDLE 		hPodMQ;
	LPCSTR 		lpcszServer;
	LPCSTR 		lpcszQueue;
	USHORT 		usPort;
	ULONGLONG	ullFlags;
	QUEUE_REQUEST	qrData;
} PODMQ_CLIENT_AUTOMATON, *LPPODMQ_CLIENT_AUTOMATON;




_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyPodMQClientAutomaton(
	_In_ 		HDERIVATIVE	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){

	return TRUE;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientOnCreate(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_CREATE), 0, NULLPTR);

	LPPODMQ_CLIENT_AUTOMATON lppmqcaData;
	lppmqcaData = OBJECT_CAST(hObject, LPPODMQ_CLIENT_AUTOMATON);

	if (NULLPTR == lppmqcaData)
	{ return NULLPTR; }

	lppmqcaData->hPodMQ = CreateMessageQueueConnection(
		lppmqcaData->lpcszServer,
		lppmqcaData->usPort,
		(lppmqcaData->ullFlags & AUTOMATON_CREATION_USE_IPV6) ? TRUE : FALSE
	);

	StringCopySafe(
		lppmqcaData->qrData.csQueue,
		lppmqcaData->lpcszQueue,
		sizeof(lppmqcaData->qrData.csQueue) - 0x01U
	);

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientOnDestroy(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_CREATE), 0, NULLPTR);

	LPPODMQ_CLIENT_AUTOMATON lppmqcaData;
	lppmqcaData = OBJECT_CAST(hObject, LPPODMQ_CLIENT_AUTOMATON);

	if (NULLPTR == lppmqcaData)
	{ return NULLPTR; }
	
	ZeroMemory(&(lppmqcaData->qrData), sizeof(QUEUE_REQUEST));
	DestroyObject(lppmqcaData->hPodMQ);

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientOnRead(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientOnWrite(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientOnTick(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientToString(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__PodMQClientToJson(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){

	return NULLPTR;
}



_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE
CreatePodMQClientAutomaton(
	_In_ 		HANDLE 		hRegistry,
	_In_Z_ 		LPCSTR RESTRICT	lpcszDeviceName,
	_In_Z_		LPCSTR RESTRICT	lpcszQueue, 
	_In_Z_ 		LPCSTR RESTRICT	lpcszServer,
	_In_ 		USHORT 		usPort,
	_In_Z_ 		DLPCSTR		dlpcstrQueuesToWriteTo,
	_In_ 		UARCHLONG	ualNumberOfWriteableQueues,
	_In_ 		LPVOID 		lpParam,
	_In_Opt_ 	ULONGLONG 	ullFlags
){
	HANDLE hAutomaton;
	LPPODMQ_CLIENT_AUTOMATON lppmqcaData;

	lppmqcaData = GlobalAllocAndZero(sizeof(PODMQ_CLIENT_AUTOMATON));
	if (NULLPTR == lppmqcaData)
	{ return NULL_OBJECT; }

	lppmqcaData->hPodMQ = CreateMessageQueueConnection(
		lpcszServer,
		usPort,
		FALSE
	);

	hAutomaton = CreateAutomatonEx(
		hRegistry,
		AUTOMATON_CLASS_PODMQ_CLIENT,
		lpcszDeviceName,
		__PodMQClientOnCreate,
		__PodMQClientOnDestroy,
		__PodMQClientOnRead,
		__PodMQClientOnWrite,
		__PodMQClientOnTick,
		__PodMQClientToString,
		__PodMQClientToJson,
		NULLPTR,
		NULLPTR,
		0,
		lppmqcaData,
		0
	);

	if (NULL_OBJECT == hAutomaton)
	{
		FreeMemory(lppmqcaData);
		return NULL_OBJECT;
	}

	lppmqcaData->hAutomaton = hAutomaton;
	return hAutomaton;
}
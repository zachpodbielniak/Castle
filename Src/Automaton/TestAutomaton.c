/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "TestAutomaton.h"
#include "AutomatonObject.c"




typedef struct __TEST_AUTOMATON
{
	INHERITS_FROM_AUTOMATON();
	LPVOID 		lpParam;
	ULONGLONG	ullFlags;
} TEST_AUTOMATON, *LPTEST_AUTOMATON;




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestOnCreate(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_CREATE), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPTEST_AUTOMATON lptaData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lptaData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestOnDestroy(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_DESTROY), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPTEST_AUTOMATON lptaData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lptaData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestOnRead(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_READ), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPTEST_AUTOMATON lptaData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lptaData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestOnWrite(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_WRITE), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPTEST_AUTOMATON lptaData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lptaData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestOnTick(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TICK), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPTEST_AUTOMATON lptaData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lptaData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestToString(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TOSTRING), 0, NULLPTR);

	LPAUTOMATON lpamDevice;
	LPTEST_AUTOMATON lptaData;
	DLPVOID dlpOut;
	LPSTR lpszOut;
	CSTRING csBuffer[1024];
	
	lpamDevice = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpamDevice, NULLPTR);
	lptaData = lpamDevice->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	dlpOut = (DLPVOID)lpData;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	/* Return New String */
	if (NULLPTR == dlpOut)
	{
		if (AUTOMATON_TOSTRING_DESCRIPTION == ullFlags)
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div><b>%p</b>-%s:%s</div>\r\n"
				"<div>Description: %s</div>\r\n"
				"<div>Lock Handle: %p</div>\r\n"
				"<div>Child Data: %p</div>\r\n"
				"<div>Construtor: %p</div>\r\n"
				"<div>Destructor: %p</div>\r\n"
				"<div>Read: %p</div>\r\n"
				"<div>Write: %p</div>\r\n"
				"<div>Tick: %p</div>\r\n"
				"<div>ToString: %p</div>\r\n"
				"<div>ToJson: %p</div>\r\n"
				"<div>Flags: %p</div>\r\n",
				lpamDevice,
				lpamDevice->lpcszClass,
				lpamDevice->lpcszName,
				lptaData->lpszDescription,
				lpamDevice->hLock,
				lpamDevice->lpChildData,
				lpamDevice->lpfnCreate,
				lpamDevice->lpfnDestroy,
				lpamDevice->lpfnRead,
				lpamDevice->lpfnWrite,
				lpamDevice->lpfnTick,
				lpamDevice->lpfnToString,
				lpamDevice->lpfnToJson,
				(LPVOID)lpamDevice->ullFlags
			);

			lpszOut = GlobalAllocAndZero(StringLength(csBuffer) + 0x01U);
			CopyMemory(lpszOut, csBuffer, StringLength(csBuffer));
			return lpszOut;
		}
		else if (AUTOMATON_TOSTRING_DESCRIPTION_LIGHT == ullFlags)
		{
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 0x01U,
				"<div>%s</div>\r\n",
				lptaData->lpszDescription
			);

			lpszOut = GlobalAllocAndZero(StringLength(csBuffer) + 0x01U);
			CopyMemory(lpszOut, csBuffer, StringLength(csBuffer));
			return lpszOut;
		}
	}

	return NULLPTR;
}




_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__TestToJson(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
){
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(ualBufferSize);
	UNREFERENCED_PARAMETER(lpualBytesWritten);
	UNREFERENCED_PARAMETER(tyDataType);
	UNREFERENCED_PARAMETER(lptyOutputType);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(ullFlags);

	EXIT_IF_UNLIKELY_NULL(hObject, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hObject), HANDLE_TYPE_AUTOMATON, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(StringCompare(lpcszEvent, AUTOMATON_EVENT_TOJSON), 0, NULLPTR);

	LPAUTOMATON lpatData;
	LPTEST_AUTOMATON lptaData;
	
	lpatData = OBJECT_CAST(hObject, LPAUTOMATON);
	EXIT_IF_UNLIKELY_NULL(lpatData, NULLPTR);
	lptaData = lpatData->lpChildData;
	EXIT_IF_UNLIKELY_NULL(lptaData, NULLPTR);

	NO_OPERATION();

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE
CreateTestAutomaton(
	_In_ 		HANDLE 		hRegistry,
	_In_Z_ 		LPCSTR RESTRICT	lpcszDeviceName,
	_In_Z_		LPCSTR RESTRICT	lpcszDescription,
	_In_Opt_	LPVOID 		lpParam,
	_In_Opt_ 	ULONGLONG 	ullFlags
){
	EXIT_IF_UNLIKELY_NULL(hRegistry, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszDeviceName, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(lpcszDescription, NULL_OBJECT);

	HANDLE hAutomaton;
	LPTEST_AUTOMATON lptaData;

	lptaData = GlobalAllocAndZero(sizeof(TEST_AUTOMATON));
	if (NULLPTR == lptaData)
	{ JUMP(__TA_FAILED); }

	lptaData->lpszDescription = GlobalAllocAndZero(StringLength(lpcszDescription) + 0x01U);
	if (NULLPTR == lpcszDescription)
	{ JUMP(__DSZ_FAILED); }
	StringCopy(lptaData->lpszDescription, lpcszDescription);

	hAutomaton = CreateAutomatonEx(
		hRegistry,
		AUTOMATON_CLASS_TEST,
		lpcszDeviceName,
		__TestOnCreate,
		__TestOnDestroy,
		__TestOnRead,
		__TestOnWrite,
		__TestOnTick,
		__TestToString,
		__TestToJson,
		NULLPTR,
		NULLPTR,
		0,
		lptaData,
		lpParam,
		ullFlags
	);

	if (NULL_OBJECT == hAutomaton)
	{ JUMP(__OBJECT_FAILED); }

	lptaData->hAutomaton = hAutomaton;
	lptaData->ullFlags = ullFlags;
	lptaData->lpParam = lpParam;
	
	return hAutomaton;

	__OBJECT_FAILED:
	FreeMemory(lptaData->lpszDescription);
	__DSZ_FAILED:
	FreeMemory(lptaData);
	__TA_FAILED:
	return NULL_OBJECT;
}


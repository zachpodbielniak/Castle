/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CASTLE_AUTOMATONOBJECT_C
#define CASTLE_AUTOMATONOBJECT_C


#include "Automaton.h"


typedef struct __AUTOMATON
{
	INHERITS_FROM_HANDLE();
	HANDLE 				hRegistry;
	HANDLE 				hLock;
	LPVOID 				lpChildData;		/* For Inherited Objects */

	LPCSTR 				lpcszClass;
	LPCSTR 				lpcszName;

	LPFN_AUTOMATON_OPERATION	lpfnCreate;
	LPFN_AUTOMATON_OPERATION	lpfnDestroy;
	LPFN_AUTOMATON_OPERATION	lpfnRead;
	LPFN_AUTOMATON_OPERATION	lpfnWrite;
	LPFN_AUTOMATON_OPERATION	lpfnTick;
	LPFN_AUTOMATON_OPERATION	lpfnToString;
	LPFN_AUTOMATON_OPERATION	lpfnToJson;

	HHASHTABLE			hhtRegisteredOperations;

	BOOL 				bTriggeredRead;
	BOOL 				bTriggeredWrite;

	LPVOID 				lpTriggeredReadData;
	LPVOID 				lpTriggeredWriteData;

	LPVOID 				lpData;
	ULONGLONG 			ullFlags;
} AUTOMATON, *LPAUTOMATON, **DLPAUTOMATON;


#endif

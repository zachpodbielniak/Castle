/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2020 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CASTLE_AUTOMATON_H
#define CASTLE_AUTOMATON_H


#include "../Castle.h"
#include "AutomatonRegistry.h"


#define AUTOMATON_EVENT_CREATE		"Create"
#define AUTOMATON_EVENT_DESTROY		"Destroy"
#define AUTOMATON_EVENT_READ		"Read"
#define AUTOMATON_EVENT_WRITE		"Write"
#define AUTOMATON_EVENT_TICK		"Tick"
#define AUTOMATON_EVENT_TOSTRING	"ToString"
#define AUTOMATON_EVENT_TOJSON		"ToJson"


#define INHERITS_FROM_AUTOMATON()		HANDLE 		hAutomaton; \
						LPSTR 		lpszDescription




/****t* Automaton/LPFN_AUTOMATON_OPERATION
 * NAME
 * 	LPFN_AUTOMATON_OPERATION -- Call back function for Automaton operations.
 * COPYRIGHT
 *	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR
 * 	Zach Podbielniak
 * SYNOPSIS
 * 	typedef _Call_Back_ LPVOID (* LPFN_AUTOMATON_OPERATION)(
 *		_In_ 		HANDLE 				hObject,
 *		_In_Z_ 		LPCSTR 				lpcszEvent,
 * 		_In_ 		LPVOID				lpData,
 *		_In_ 		UARCHLONG 			ualBufferSize,
 *		_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
 *		_In_Opt_ 	TYPE 				tyDataType,
 *		_Out_Opt_	LPTYPE 				lptyOutputType,
 *		_In_Opt_ 	LPVOID 				lpParam,
 *		_In_Opt_ 	ULONGLONG			ullFlags
 *	);
 *
 * 	typedef LPFN_AUTOMATON_OPERATION 	*DLPFN_AUTOMATON_OPERATION, **TLPFN_AUTOMATON_OPERATION;
 * DESCRIPTION
 * 	A call back function pointer type which can be used for Automaton
 * 	Operations, such as READ, WRITE, TICK, etc. Also includes definitions
 * 	for teh Double and Triple pointers:
 * 	- DLPFN_AUTOMATON_OPERATION
 * 	- TLPFN_AUTOMATON_OPERATION
 ******
 *
 */
typedef _Call_Back_ LPVOID (* LPFN_AUTOMATON_OPERATION)(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
);

typedef LPFN_AUTOMATON_OPERATION 	*DLPFN_AUTOMATON_OPERATION, **TLPFN_AUTOMATON_OPERATION;




typedef enum __AUTOMATON_CREATION_FLAGS
{
	AUTOMATON_CREATION_USE_IPV6		= 1 << 30,
	AUTOMATON_CREATION_DEFERRED		= 1 << 31
} AUTOMATON_CREATION_FLAGS;




typedef enum __AUTOMATON_READ_FLAGS
{
	AUTOMATON_READ_TRIGGERED_BY_TICK	= 1 << 0
} AUTOMATON_READ_FLAGS;




typedef enum __AUTOMATON_WRITE_FLAGS
{
	AUTOMATON_WRITE_INVERSE			= 1 << 0,
	AUTOMATON_WRITE_TRIGGERED_BY_TICK	= 1 << 1,
	AUTOMATON_WRITE_DEVICE_OPERATION	= 1 << 8,
} AUTOMATON_WRITE_FLAGS;




typedef enum __AUTOMATON_TOSTRING_FLAGS
{
	AUTOMATON_TOSTRING_DESCRIPTION		= 1 << 0,
	AUTOMATON_TOSTRING_DESCRIPTION_LIGHT	= 1 << 1,
	AUTOMATON_TOSTRING_DESCRIPTION_MQ	= 1 << 2
} AUTOMATON_TOSTRING_FLAGS;



/****f* Automaton/CreateAutomaton
 * NAME
 * 	CreateAutomaton -- Create Automaton Object. Inherits from HANDLE.
 * COPYRIGHT
 *	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR
 * 	Zach Podbielniak
 * SYNOPSIS
 * 	_Success_(return != NULL_OBJECT, _Non_Locking_)
 * 	CASTLE_API
 * 	HANDLE
 * 	CreateAutomaton(
 * 		_In_ 		LPCSTR 				lpcszClass,
 *		_In_ 		LPCSTR 				lpcszName,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnCreate,	
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnDestroy,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnRead,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnWrite,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnTick,
 *		_In_ 		LPVOID 				lpParam,
 *		_In_ 		ULONGLONG 			ullFlags
 * 	);
 * FUNCTION
 * 	Creates a new Automaton object, with appropriate callbacks that define
 * 	the object itself. This is a base object, which by itself is meaningless.
 * 	Instead, objects should inherit from this.
 * INPUTS
 * 	lpcszClass:
 * 		Class Name (Defines the type) of type LPCSTR.
 * 	lpcszName:
 * 		The device name of type LPCSTR.
 * 	lpfnCreate:
 * 		The "Create" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnDestroy:
 * 		The "Destroy" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnRead:
 * 		The "Read" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnWrite:
 * 		The "Write" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnTick:
 * 		The "Tick" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpParam:
 * 		User data, of type LPVOID.
 * 	ullFlags:
 * 		Any flags, OR'd together as a ULONGLONG.
 * RESULT
 * 	HANDLE -- A HANDLE to the underlying Automaton Object state. Returns NULL_OBJECT on error.
 * SEE ALSO
 * 	CreateAutomatonEx, LPFN_AUTOMATON_OPERATION
 ******
 *
 */
_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE 
CreateAutomaton(
	_In_ 		HANDLE 				hRegistry,	/* Automaton Registry */
	_In_ 		LPCSTR 				lpcszClass,	/* Device Class */
	_In_ 		LPCSTR 				lpcszName,	/* Device Name */
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnCreate,	
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnDestroy,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnRead,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnWrite,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnTick,
	_In_ 		LPVOID 				lpChildData,
	_In_ 		LPVOID 				lpParam,	/* To Be Passed To All Callbacks */
	_In_ 		ULONGLONG 			ullFlags	/* Operation Flags */
);




/****f* Automaton/CreateAutomatonEx
 * NAME
 * 	CreateAutomatonEx -- Create Extended Automaton Object. Inherits from HANDLE.
 * COPYRIGHT
 *	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR
 * 	Zach Podbielniak
 * SYNOPSIS
 * 	_Success_(return != NULL_OBJECT, _Non_Locking_)
 * 	CASTLE_API
 * 	HANDLE
 * 	CreateAutomatonEx(
 * 		_In_ 		LPCSTR 				lpcszClass,
 *		_In_ 		LPCSTR 				lpcszName,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnCreate,	
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnDestroy,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnRead,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnWrite,
 *		_In_ 		LPFN_AUTOMATON_OPERATION	lpfnTick,
 *		_In_Opt_	LPFN_AUTOMATON_OPERATION	lpfnToString,
 *		_In_Opt_	LPFN_AUTOMATON_OPERATION	lpfnToJson,
 *		_In_Opt_ 	DLPFN_AUTOMATON_OPERATION	dlpfnOperations,
 *		_In_Opt_Z_ 	DLPCSTR 			dlpcszEventsForOperations,
 *		_In_Opt_ 	UARCHLONG 			ualNumberOfExtraEvents,
 *		_In_ 		LPVOID 				lpParam,
 *		_In_ 		ULONGLONG 			ullFlags
 * 	);
 * FUNCTION
 * 	Creates a new, extended Automaton object, with appropriate callbacks that define
 * 	the object itself. This is a base object, which by itself is meaningless.
 * 	Instead, objects should inherit from this. This extended call allows for definining
 * 	the ToString, ToJson, and other LPFN_AUTOMATON_OPERATION's by using the passed in
 * 	dlpfnOperations, with the appropriate Events with dlpcszEventsForOperations.
 * INPUTS
 * 	lpcszClass:
 * 		Class Name (Defines the type) of type LPCSTR.
 * 	lpcszName:
 * 		The device name of type LPCSTR.
 * 	lpfnCreate:
 * 		The "Create" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnDestroy:
 * 		The "Destroy" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnRead:
 * 		The "Read" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnWrite:
 * 		The "Write" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnTick:
 * 		The "Tick" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnToString:
 * 		The "ToString" callback of type LPFN_AUTOMATON_OPERATION.
 * 	lpfnToJson:
 * 		The "ToJson" callback of type LPFN_AUTOMATON_OPERATION.
 * 	dlpfnOperations:
 * 		List of LPFN_AUTOMATON_OPERATION's, mapping to dlpcszEventsForOperations.
 * 	dlpcszEventsForOperations:
 * 		List of the Event Names for the dlpfnOperations Events.
 * 	ualNumberOfExtraEvents:
 * 		The length of dlpfnOperations (and dlpcszEventsForOperations).
 * 	lpParam:
 * 		User data, of type LPVOID.
 * 	ullFlags:
 * 		Any flags, OR'd together as a ULONGLONG.
 * RESULT
 * 	HANDLE -- A HANDLE to the underlying Automaton Object state. Returns NULL_OBJECT on error.
 * SEE ALSO
 * 	CreateAutomaton, LPFN_AUTOMATON_OPERATION
 ******
 *
 */
_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE 
CreateAutomatonEx(
	_In_ 		HANDLE 				hRegistry,			/* Automaton Registry */
	_In_ 		LPCSTR 				lpcszClass,			/* Device Class */
	_In_ 		LPCSTR 				lpcszName,			/* Device Name */
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnCreate,	
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnDestroy,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnRead,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnWrite,
	_In_ 		LPFN_AUTOMATON_OPERATION	lpfnTick,
	_In_Opt_	LPFN_AUTOMATON_OPERATION	lpfnToString,
	_In_Opt_	LPFN_AUTOMATON_OPERATION	lpfnToJson,
	_In_Opt_ 	DLPFN_AUTOMATON_OPERATION	dlpfnOperations,
	_In_Opt_Z_ 	DLPCSTR 			dlpcszEventsForOperations,
	_In_Opt_ 	UARCHLONG 			ualNumberOfExtraEvents,
	_In_ 		LPVOID 				lpChildData,
	_In_ 		LPVOID 				lpParam,			/* To Be Passed To All Callbacks */
	_In_ 		ULONGLONG 			ullFlags			/* Operation Flags */
);




/****f* Automaton/AutomatonGetData
 * NAME
 * 	AutomatonGetData -- Return a pointer to the data.
 * COPYRIGHT
 * 	Copyright (C) 2020 Zach Podbielniak 
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS 
 *	_Success_(return != NULLPTR, _Interlocked_Operation_)
 *	CASTLE_API
 *	LPVOID
 *	AutomatonGetData(
 *		_In_ 		HANDLE 				hObject
 *	);
 * FUNCTION 
 * 	Returns a pointer (LPVOID) to the data stored in the Object.
 * INPUTS 
 * 	hObject:
 * 		A HANDLE to an instance of an Automaton Object.
 * RESULT
 * 	LPVOID -- A generic local pointer to the data.
 ******
 *
 */
_Success_(return != NULLPTR, _Interlocked_Operation_)
CASTLE_API
LPVOID
AutomatonGetData(
	_In_ 		HANDLE 				hObject
);




/****f* Automaton/AutomatonSetData
 * NAME
 * 	AutomatonSetData -- Set the data for the Automaton object.
 * COPYRIGHT
 * 	Copyright (C) 2020 Zach Podbielniak 
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS 
 *	_Success_(return != FALSE, _Interlocked_Operation_)
 *	CASTLE_API
 *	LPVOID
 *	AutomatonSetData(
 *		_In_ 		HANDLE 				hObject
 *		_In_ 		LPVOID 				lpData
 *	);
 * FUNCTION 
 * 	Sets the pointer value of the data for the Automaton Object.
 * INPUTS 
 * 	hObject:
 * 		A HANDLE to an instance of an Automaton Object.
 * 	lpData:
 * 		A generic local pointer (LPVOID) to the data.
 * RESULT
 * 	BOOL -- FALSE indicates there was an error. !(FALSE) was a success.
 ******
 *
 */
_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL
AutomatonSetData(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPVOID 				lpData
);




/****f* Automaton/AutomatonRegisterOperation
 * NAME
 * 	AutomatonRegisterOperation -- Register a LPFN_AUTOMATON_OPERATION tied to an event.
 * COPYRIGHT
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS
 *	_Success_(return != FALSE, _Interlocked_Operation_)
 *	CASTLE_API
 *	BOOL 
 *	AutomatonRegisterOperation(
 *		_In_ 		HANDLE 				hObject,
 *		_In_ 		LPCSTR 				lpcszEvent,
 *		_In_		LPFN_AUTOMATON_OPERATION	lpfnOperation
 *	);
 * FUNCTION 
 * 	Registers an LPFN_AUTOMATON_OPERATION to occur, when AutomatonExec is called with the 
 * 	event that is passed into lpcszEvent. Only events that are not defined as "Create", "Destroy",
 * 	"Read", "Write", "Tick", "ToString", and "ToJson" are to be defined using this function.
 * INPUTS
 * 	hObject:
 * 		A HANDLE to an instance of an Automaton Object.
 * 	lpcszEvent:
 * 		A local pointer to a constant string that is null-terminated that is the
 * 		event name.
 * 	lpfnOperation:
 * 		A local pointer to a function pointer of type LPFN_AUTOMATON_OPERATION for the call back.
 * RESULT 
 * 	BOOL -- FALSE indicates there was an error. !(FALSE) was a success.
 * SEE ALSO 
 * 	AutomatonExec
 ******
 *
 */
_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API
BOOL 
AutomatonRegisterOperation(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPCSTR 				lpcszEvent,
	_In_		LPFN_AUTOMATON_OPERATION	lpfnOperation
);




/****f* Automaton/AutomatonRead
 * NAME
 * 	AutomatonRead -- Read from an Automaton Object.
 * COPYRIGHT
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS
 *	_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
 *	CASTLE_API 
 *	LPVOID 
 *	AutomatonRead(
 *		_In_ 		HANDLE 				hObject,
 *		_Out_ 		LPVOID 				lpOut,
 *		_In_ 		UARCHLONG 			ualOutBufferSize,
 *		_In_Opt_ 	LPVOID 				lpParam
 *	);
 * FUNCTION 
 * 	Reads from an Automaton Object, storing the result in lpOut. If ualOutBufferSize is 
 * 	less than the size of the read, the function will fail. Be sure to verify return.
 * INPUTS
 * 	hObject:
 * 		A HANDLE to an instance of an Automaton Object.
 * 	lpOut:
 * 		A local pointer to a buffer to store the output.
 * 	ualOutBufferSize:
 * 		Size of buffer pointed to by lpOut.
 * 	lpParam:
 * 		Optional paramater. Depensds on implementation.
 * RESULT 
 * 	LPVOID -- Result depends on implementation. On error, the value will be (UARCHLONG)-1.
 * SEE ALSO 
 * 	AutomatonReadEx
 ******
 *
 */
_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID 
AutomatonRead(
	_In_ 		HANDLE 				hObject,
	_Out_ 		LPVOID 				lpOut,
	_In_ 		UARCHLONG 			ualOutBufferSize,
	_In_Opt_ 	LPVOID 				lpParam
);




/****f* Automaton/AutomatonReadEx
 * NAME
 * 	AutomatonReadEx -- Read from an Automaton Object, with more options.
 * COPYRIGHT
 * 	Copyright (C) 2020 Zach Podbielniak
 * AUTHOR 
 * 	Zach Podbielniak 
 * SYNOPSIS
 *	_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
 *	CASTLE_API 
 *	LPVOID 
 *	AutomatonRead(
 *		_In_ 		HANDLE 				hObject,
 *		_Out_ 		LPVOID 				lpOut,
 *		_In_ 		UARCHLONG 			ualOutBufferSize,
 *		_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
 *		_Out_Opt_	LPTYPE 				lptyOutputType,
 *		_In_Opt_ 	LPVOID 				lpParam,
 *		_In_Opt_ 	ULONGLONG 			ullFlags
 *	);
 * FUNCTION 
 * 	Reads from an Automaton Object, storing the result in lpOut. If ualOutBufferSize is 
 * 	less than the size of the read, the function will fail. Be sure to verify return.
 * INPUTS
 * 	hObject:
 * 		A HANDLE to an instance of an Automaton Object.
 * 	lpOut:
 * 		A local pointer to a buffer to store the output.
 * 	ualOutBufferSize:
 * 		Size of buffer pointed to by lpOut.
 * 	lpualBytesWritten:
 * 		A pointer to a UARCHLONG to store the size written to lpOut buffer by the Read.
 * 	lptyOutputType:
 * 		A pointer to a TYPE that can store the underlying TYPE of data written to lpOut.
 * 	lpParam:
 * 		Optional paramater. Depensds on implementation.
 * 	ullFlags:
 * 		A ULONGLONG OR'd with flags. Reserved for future use.
 * RESULT 
 * 	LPVOID -- Result depends on implementation. On error, the value will be (UARCHLONG)-1.
 * SEE ALSO 
 * 	AutomatonReadEx
 ******
 *
 */
_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID 
AutomatonReadEx(
	_In_ 		HANDLE 				hObject,
	_Out_ 		LPVOID 				lpOut,
	_In_ 		UARCHLONG 			ualOutBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID
AutomatonWrite(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize,
	_In_Opt_ 	LPVOID 				lpParam
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID
AutomatonWriteEx(
	_In_ 		HANDLE 				hObject,
	_In_ 		LPVOID 				lpData,
	_In_ 		UARCHLONG 			ualDataSize,
	_In_Opt_ 	TYPE 				tyData,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API
LPVOID
AutomatonTick(
	_In_ 		HANDLE 				hObject,
	_In_Opt_	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API
LPVOID 
AutomatonToString(
	_In_ 		HANDLE 				hObject,
	_Out_ 		DLPVOID 			dlpOut,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API
LPVOID 
AutomatonToJson(
	_In_ 		HANDLE 				hObject,
	_Out_ 		DLPVOID 			dlpOut,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG 			ullFlags
);




_Success_(return != (UARCHLONG)-1, _Interlocked_Operation_)
CASTLE_API 
LPVOID
AutomatonExec(
	_In_ 		HANDLE 				hObject,
	_In_Z_ 		LPCSTR 				lpcszEvent,
	_In_ 		LPVOID				lpData,
	_In_ 		UARCHLONG 			ualBufferSize,
	_Out_Opt_	LPUARCHLONG 			lpualBytesWritten,
	_In_Opt_ 	TYPE 				tyDataType,
	_Out_Opt_	LPTYPE 				lptyOutputType,
	_In_Opt_ 	LPVOID 				lpParam,
	_In_Opt_ 	ULONGLONG			ullFlags
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
CASTLE_API 
LPCSTR 
AutomatonGetName(
	_In_ 		HANDLE 				hObject
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
CASTLE_API 
LPCSTR 
AutomatonGetClass(
	_In_ 		HANDLE 				hObject
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API 
BOOL 
AutomatonTriggeredRead(
	_In_ 		HANDLE 				hObject
);




_Success_(return != FALSE, _Interlocked_Operation_)
CASTLE_API 
BOOL 
AutomatonTriggeredWrite(
	_In_ 		HANDLE 				hObject
);




_Success_(return != NULL_OBJECT, _Interlocked_Operation_)
CASTLE_API
HANDLE 
AutomatonGetRegistry(
	_In_ 		HANDLE 				hObject
);




#endif

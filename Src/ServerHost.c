/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "Server/CastleServer.h"
#ifdef BUILD_WITH_ONEAGENT_SDK
#include <onesdk/onesdk.h>
#endif


_Can_Kill_Process_
INTERNAL_OPERATION
VOID
SignalHandler(
	_In_ 		LONG 		lSignal
){
	if (SIGINT == lSignal)
	{ exit(0); }
}




_Success_(return != 0, _Non_Locking_)
LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hServer;

	signal(SIGINT, SignalHandler);

	#ifdef BUILD_WITH_ONEAGENT_SDK
	if (ONESDK_SUCCESS != onesdk_initialize_2(0))
	{ PostQuitMessage(254); }
	#endif
	
	SetThreadName(GetCurrentThread(), "CastleHost");

	hServer = CreateCastleServer("CastleConfig.ini", 0);
	LongSleep(INFINITE_WAIT_TIME);

	return 0;
}

/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef CASTLE_CASTLESERVER_H
#define CASTLE_CASTLESERVER_H


#include "../Automaton/Automaton.h"
#include "../Automaton/AutomatonRegistry.h"
#include "../Automaton/AutomatonRest.h"

#include "../Automaton/TestAutomaton.h"
#include "../Automaton/TextAutomaton.h"
#include "../Automaton/PublicIPClientAutomaton.h"
#include "../Automaton/SwitchableDeviceAutomaton.h"




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE 
CreateCastleServer(
	_In_Z_		LPCSTR 		lpcszConfigFile,
	_In_Opt_	ULONGLONG 	ullFlags
);



#endif
/*
  ____          _   _      
 / ___|__ _ ___| |_| | ___ 
| |   / _` / __| __| |/ _ \
| |__| (_| \__ \ |_| |  __/
 \____\__,_|___/\__|_|\___|

Extensible Automation Orchestration.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "CastleServer.h"



typedef struct __CASTLE_SERVER
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hMainRegistry;
	HVECTOR 		hvOtherRegistries;
	HANDLE 			hHttpServer;
	HANDLE 			hConfigFile;
	HANDLE 			hLock;
	HANDLE 			hPodMqServer;
	HANDLE 			hPodMqConnection;
	HANDLE 			hTickThread;

	/* HTTP Server Options From Parsed File */
	LPSTR 			lpszName;
	LPSTR 			lpszHostName;
	DLPSTR 			dlpszAddresses;	/* Created from SplitString */
	LPUSHORT 		lpusPorts;
	UARCHLONG 		ualNumberOfAddresses;
	UARCHLONG		ualNumberOfPorts;
	UARCHLONG 		ualWorkerThreadCount;
	UARCHLONG 		ualHashSize;
	
	
} CASTLE_SERVER, *LPCASTLE_SERVER, **DLPCASTLE_SERVER;



typedef struct __INI_AUTOMATON_SECTION
{
	HANDLE 			hRegistry;
	HANDLE 			hAutomaton;

	LPSTR 			lpszClass;
	LPSTR 			lpszName;
	LPSTR 			lpszDescription;
	LPSTR 			lpszOptions;

	/* PodMQ Read And Write Ops */
	DLPSTR 			dlpszWritableQueues;
	DLPSTR 			dlpszReadableQueues;
	UARCHLONG 		ualNumberOfWritableQueues;
	UARCHLONG		ualNumberOfReadableQueues;

	ULONGLONG 		ullFlags;
} INI_AUTOMATON_SECTION, *LPINI_AUTOMATON_SECTION;




_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyCastleServer(
	_In_ 		HDERIVATIVE	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	UNREFERENCED_PARAMETER(hDerivative);
	return TRUE;
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__CreateAutomatonFromIniSection(
	_In_ 		LPINI_AUTOMATON_SECTION		lpiasDescriptor
){
	EXIT_IF_UNLIKELY_NULL(lpiasDescriptor, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpiasDescriptor->hRegistry, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpiasDescriptor->lpszClass, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpiasDescriptor->lpszName, FALSE);
		
	DLPSTR dlpszOptions;
	UARCHLONG ualCount;

	if (0 == StringCompare(AUTOMATON_CLASS_TEST, lpiasDescriptor->lpszClass))
	{
		lpiasDescriptor->hAutomaton = CreateTestAutomaton(
			lpiasDescriptor->hRegistry,
			(LPCSTR)lpiasDescriptor->lpszName,
			(LPCSTR)lpiasDescriptor->lpszDescription,
			NULLPTR,
			lpiasDescriptor->ullFlags
		);
	}
	
	else if (0 == StringCompare(AUTOMATON_CLASS_TEXT, lpiasDescriptor->lpszClass))
	{
		lpiasDescriptor->hAutomaton = CreateTextAutomaton(
			lpiasDescriptor->hRegistry,
			(LPCSTR)lpiasDescriptor->lpszName,
			(LPCSTR)lpiasDescriptor->lpszDescription,
			NULLPTR,
			lpiasDescriptor->ullFlags
		);
	}

	else if (0 == StringCompare(AUTOMATON_CLASS_PUBLICIP_CLIENT, lpiasDescriptor->lpszClass))
	{
		LPSTR lpszServer;
		LPSTR lpszPort;
		LPSTR lpszUri;
		LPSTR lpszHttps;
		LPSTR lpszVerify;
		BOOL bHttps;
		BOOL bVerify;
		USHORT usPort;

		if (NULLPTR == lpiasDescriptor->lpszOptions)
		{ return FALSE; }

		ualCount = StringSplit(lpiasDescriptor->lpszOptions, ";", &dlpszOptions);

		if (5 != ualCount)
		{
			WriteFile(GetStandardError(), "Incorrect usage for PUBLIC_IP_AUTOMATON:\n", 0);
			WriteFile(GetStandardError(), "Format: <hostname>;<port>;<uri>;<use_https_1_or_0>;<verify_cert_1_or_0>\n", 0);
			return FALSE;
		}

		lpszServer = dlpszOptions[0];
		lpszPort = dlpszOptions[1];
		lpszUri = dlpszOptions[2];
		lpszHttps = dlpszOptions[3];
		lpszVerify = dlpszOptions[4];

		StringScanFormat(
			lpszHttps,
			"%hhu",
			&bHttps
		);

		StringScanFormat(
			lpszVerify,
			"%hhu",
			&bVerify
		);

		StringScanFormat(
			lpszPort,
			"%hu",
			&usPort
		);

		lpiasDescriptor->hAutomaton = CreatePublicIPClientAutomaton(
			lpiasDescriptor->hRegistry,
			lpiasDescriptor->lpszName,
			lpszServer,
			lpszUri,
			bHttps,
			bVerify,
			usPort,
			(DLPCSTR)lpiasDescriptor->dlpszWritableQueues,
			lpiasDescriptor->ualNumberOfWritableQueues,
			NULLPTR,
			lpiasDescriptor->ullFlags
		);

		DestroySplitString(dlpszOptions);
	}

	else if (0 == StringCompare(AUTOMATON_CLASS_SWITCHABLE_DEVICE, lpiasDescriptor->lpszClass))
	{
		lpiasDescriptor->hAutomaton = CreateSwitchableDeviceAutomaton(
			lpiasDescriptor->hRegistry,
			(LPCSTR)lpiasDescriptor->lpszName,
			(LPCSTR)lpiasDescriptor->lpszDescription,
			(DLPCSTR)lpiasDescriptor->dlpszWritableQueues,
			lpiasDescriptor->ualNumberOfWritableQueues,
			(DLPCSTR)lpiasDescriptor->dlpszReadableQueues,
			lpiasDescriptor->ualNumberOfReadableQueues,
			lpiasDescriptor->lpszOptions,
			NULLPTR,
			NULLPTR,
			lpiasDescriptor->ullFlags
		);
	}

	return TRUE;
}



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ParseIniResults(
	_In_ 		LPCASTLE_SERVER	lpasServer
){
	EXIT_IF_UNLIKELY_NULL(lpasServer, FALSE);

	UARCHLONG ualIndex;
	UARCHLONG ualJndex;
	HVECTOR hvSections;
	LPINI_SECTION_VECTOR lpisvIndex;
	INI_AUTOMATON_SECTION iasDevice;
	BOOL bCreateAutomaton;

	hvSections = GetIniParseResultVector(lpasServer->hConfigFile);
	if (NULLPTR == hvSections)
	{ return FALSE; }

	for (
		ualIndex = 0;
		ualIndex < VectorSize(hvSections);
		ualIndex++
	){
		bCreateAutomaton = FALSE;
		ZeroMemory(&iasDevice, sizeof(INI_AUTOMATON_SECTION));

		lpisvIndex = VectorAt(hvSections, ualIndex);
		if (NULLPTR == lpisvIndex)
		{ continue; }

		for (
			ualJndex = 0;
			ualJndex < VectorSize(lpisvIndex->hvEntries);
			ualJndex++
		){
			LPINI_ENTRY lpieData;
			lpieData = VectorAt(lpisvIndex->hvEntries, ualJndex);
			if (NULLPTR == lpieData)
			{ continue; }

			if (0 == StringCompare("HttpServer", lpisvIndex->lpszSection))
			{
				/* Server Name */
				if (0 == StringCompare("Name", lpieData->lpszKey))
				{
					UARCHLONG ualLength;

					ualLength = StringLength(lpieData->lpszValue); 
					lpasServer->lpszName = LocalAllocAndZero(ualLength + 0x01U);

					if (NULLPTR == lpasServer->lpszName) 
					{ return FALSE; }

					CopyMemory(lpasServer->lpszName, lpieData->lpszValue, ualLength + 0x01U);
				}

				/* Host Name */
				else if (0 == StringCompare("HostName", lpieData->lpszKey))
				{
					UARCHLONG ualLength;

					ualLength = StringLength(lpieData->lpszValue);
					lpasServer->lpszHostName = LocalAllocAndZero(ualLength + 0x01U);

					if (NULLPTR == lpasServer->lpszName)
					{ return FALSE; }

					CopyMemory(lpasServer->lpszHostName, lpieData->lpszValue, ualLength);
				}

				/* Bind Addresses */
				else if (0 == StringCompare("BindAddresses", lpieData->lpszKey))
				{ lpasServer->ualNumberOfAddresses = StringSplit(lpieData->lpszValue, ",", &(lpasServer->dlpszAddresses)); }

				/* BindPorts */
				else if (0 == StringCompare("BindPorts", lpieData->lpszKey))
				{
					UARCHLONG ualKndex;
					DLPSTR dlpszSplit;

					lpasServer->ualNumberOfPorts = StringSplit(lpieData->lpszValue, ",", &dlpszSplit);
					lpasServer->lpusPorts = LocalAllocAndZero(sizeof(SHORT) * lpasServer->ualNumberOfPorts);

					for (
						ualKndex = 0;
						ualKndex < lpasServer->ualNumberOfPorts;
						ualKndex++
					){ 
						StringScanFormat(
							dlpszSplit[ualKndex], 
							"%hu", 
							&(lpasServer->lpusPorts[ualKndex])
						);
					}

					DestroySplitString(dlpszSplit);
				}

				/* Worker Threads */
				else if (0 == StringCompare("WorkerThreads", lpieData->lpszKey))
				{
					if (NULLPTR == lpieData->lpszValue)
					{ continue; }

					StringScanFormat(
						lpieData->lpszValue,
						"%lu",
						&(lpasServer->ualWorkerThreadCount)						
					);
				}
				
				/* Hash Size */
				else if (0 == StringCompare("HashSize", lpieData->lpszKey))
				{
					if (NULLPTR == lpieData->lpszValue)
					{ continue; }
					
					StringScanFormat(
						lpieData->lpszValue,
						"%lu",
						&(lpasServer->ualHashSize)	
					);
				}
			}
			/* Embedded PodMQ Server */
			else if (0 == StringCompare("PodMQ", lpisvIndex->lpszSection))
			{
				QUEUE_REQUEST qrData;
				ZeroMemory(&qrData, sizeof(qrData));

				if (0 == StringCompare("Queue", lpieData->lpszKey))
				{
					StringCopySafe(
						qrData.csQueue,
						lpieData->lpszValue,
						sizeof(qrData.csQueue)
					);

					SendMessageQueueConnectionRequest(
						lpasServer->hPodMqConnection,
						CREATE_QUEUE,
						&qrData
					);
				}
			}
			/* Create Registries */
			else if (0 == StringCompare("Registry", lpisvIndex->lpszSection))
			{
				if (0 == StringCompare("Name", lpieData->lpszKey))
				{ CreateAutomatonRegistry((LPCSTR)lpieData->lpszValue); }
			}
			/* Create Automaton */
			else if (0 == StringCompare("Automaton", lpisvIndex->lpszSection))
			{
				bCreateAutomaton = TRUE;

				/* Class */
				if (0 == StringCompare("Class", lpieData->lpszKey))
				{ iasDevice.lpszClass = lpieData->lpszValue; }
				/* Name */
				else if (0 == StringCompare("Name", lpieData->lpszKey))
				{ iasDevice.lpszName = lpieData->lpszValue; }
				/* Description */
				else if (0 == StringCompare("Description", lpieData->lpszKey))
				{ iasDevice.lpszDescription = lpieData->lpszValue; }
				/* Options */
				else if (0 == StringCompare("Options", lpieData->lpszKey))
				{ iasDevice.lpszOptions = lpieData->lpszValue; }
				/* Flags */
				else if (0 == StringCompare("Flags", lpieData->lpszKey))
				{
					StringScanFormat(
						lpieData->lpszValue,
						"%lu",
						&(iasDevice.ullFlags)
					);
				}
				/* PodMQ Writable Queues */
				else if (0 == StringCompare("WritableQueues", lpieData->lpszKey))
				{ 
					iasDevice.ualNumberOfWritableQueues = StringSplit(
						lpieData->lpszValue,
						";",
						&(iasDevice.dlpszWritableQueues)
					);
				}
				/* PodMQ Readable Queues */
				else if (0 == StringCompare("ReadableQueues", lpieData->lpszKey))
				{ 
					iasDevice.ualNumberOfReadableQueues = StringSplit(
						lpieData->lpszValue,
						";",
						&(iasDevice.dlpszReadableQueues)
					);
				}
				/* Device Registry */
				else if (0 == StringCompare("Registry", lpieData->lpszKey))
				{
					if (0 == StringCompare("Default", lpieData->lpszValue))
					{ iasDevice.hRegistry = AutomatonGetRegistryByName(NULLPTR); }
					else 
					{
						iasDevice.hRegistry = AutomatonGetRegistryByName(lpieData->lpszValue);

						if (NULL_OBJECT == iasDevice.hRegistry)
						{
							WriteFile(GetStandardError(), "Registry could not be found: ", 0);
							WriteFile(GetStandardError(), lpieData->lpszValue, 0);
							WriteFile(GetStandardError(), "!\n", 0);
							PostQuitMessage(5);
						}
					}
				}
			}
			else
			{ continue; }
		}

		if (TRUE == bCreateAutomaton)
		{ __CreateAutomatonFromIniSection(&iasDevice); }
	}
	
	return TRUE;
}



_Success_(return == NULLPTR, _Interlocked_Operation_)
INTERNAL_OPERATION
LPVOID
__CastleTickThread(
	_In_Z_ 		LPVOID 		lpParam
){
	EXIT_IF_UNLIKELY_NULL(lpParam, (LPVOID)-1);
	
	LPCASTLE_SERVER lpasServer;
	lpasServer = lpParam;

	INFINITE_LOOP()
	{
		TIMESPEC tmStart;
		ULONGLONG ullSeconds;

		HighResolutionClock(&tmStart);
		AutomatonRegistryPerformTickOnAll(lpasServer->hMainRegistry);

		ullSeconds = HighResolutionClockAndGetSeconds(&tmStart);
		if (30 > ullSeconds)
		{ LongSleep(30 - ullSeconds); }
	}

	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CASTLE_API
HANDLE 
CreateCastleServer(
	_In_Z_		LPCSTR 		lpcszConfigFile,
	_In_Opt_	ULONGLONG 	ullFlags
){
	UNREFERENCED_PARAMETER(ullFlags);
	EXIT_IF_UNLIKELY_NULL(lpcszConfigFile, NULL_OBJECT);

	HANDLE hServer;
	LPCASTLE_SERVER lpasServer;

	lpasServer = GlobalAllocAndZero(sizeof(CASTLE_SERVER));
	if (NULLPTR == lpasServer)
	{ JUMP(__ALLOC_FAILED); }

	lpasServer->hLock = CreateCriticalSectionAndSpecifySpinCount(0x0C00);
	if (NULL_OBJECT == lpasServer->hLock)
	{ JUMP(__CRITSEC_FAILED); }

	lpasServer->hMainRegistry = CreateAutomatonRegistry(NULLPTR);
	if (NULL_OBJECT == lpasServer->hMainRegistry)
	{ JUMP(__REGISTRY_FAILED); }

	lpasServer->hConfigFile = OpenIniFile(lpcszConfigFile, 0);
	if (NULL_OBJECT == lpasServer->hConfigFile)
	{ JUMP(__INIPARSE_FAILED); }
	
	lpasServer->hPodMqServer = CreateMessageQueueServer(
		"0.0.0.0",
		9002,
		65534
	);

	if (NULL_OBJECT == lpasServer->hPodMqServer)
	{ JUMP(__PODMQ_SERVER_FAILED); }

	lpasServer->hPodMqConnection = CreateMessageQueueConnection(
		"127.0.0.1",
		9002,
		FALSE
	);

	if (NULL_OBJECT == lpasServer->hPodMqConnection)
	{ JUMP(__PODMQ_CONN_FAILED); }

	if (FALSE == ParseIniFile(lpasServer->hConfigFile, PARSE_INI_USE_VECTORS))
	{ JUMP(__INIPARSE_FAILED); }

	if (FALSE == __ParseIniResults(lpasServer))
	{ JUMP(__INIPARSE_FAILED); }

	lpasServer->hHttpServer = CreateHttpServerEx(
		lpasServer->lpszName,
		(DLPCSTR)lpasServer->dlpszAddresses,
		lpasServer->ualNumberOfAddresses,
		lpasServer->lpusPorts,
		lpasServer->ualNumberOfPorts,
		(LPCSTR)lpasServer->lpszHostName,
		lpasServer->ualHashSize,
		lpasServer->ualWorkerThreadCount,
		0
	);

	if (NULL_OBJECT == lpasServer->hHttpServer)
	{ JUMP(__HTTPSERVER_FAILED); }



	AutomatonRegisterRestEndpoints(
		lpasServer->hHttpServer,
		lpasServer->hMainRegistry
	);

	/* Embed PodMQ Endpoint For Device Communications Over HTTP */
	RegisterPodMQUriHandlersToHttpServer(
		lpasServer->hHttpServer,
		lpasServer->hPodMqConnection,
		"/MQ",
		NULLPTR
	);

	hServer = CreateHandleWithSingleInheritor(
		lpasServer,
		&(lpasServer->hThis),
		HANDLE_TYPE_CASTLE_SERVER,
		__DestroyCastleServer,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpasServer->ullId),
		0
	);

	if (NULL_OBJECT == hServer)
	{ JUMP(__OBJECT_FAILED); }

	lpasServer->hTickThread = CreateThread(__CastleTickThread, lpasServer, NULLPTR);
	if (NULL_OBJECT == lpasServer->hTickThread)
	{ JUMP(__TICK_THREAD_FAILED); }

	SetThreadName(lpasServer->hTickThread, "CastleTick");

	return hServer;

	__TICK_THREAD_FAILED:
	DestroyObject(hServer);
	__OBJECT_FAILED:
	__AUTOMATON_REGISTER_FAILED:
	DestroyObject(lpasServer->hHttpServer);
	__HTTPSERVER_FAILED:
	__INIPARSE_FAILED:
	DestroyObject(lpasServer->hConfigFile);
	__OPENINI_FAILED:
	__PODMQ_CONN_FAILED:
	DestroyObject(lpasServer->hPodMqServer);
	__PODMQ_SERVER_FAILED:
	DestroyObject(lpasServer->hPodMqConnection);
	DestroyObject(lpasServer->hMainRegistry);
	__REGISTRY_FAILED:
	DestroyObject(lpasServer->hLock);
	__CRITSEC_FAILED:
	FreeMemory(lpasServer);
	__ALLOC_FAILED:
	return NULLPTR;
}



